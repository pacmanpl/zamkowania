dialogImage = love.graphics.newImage("gfx/talk-pop-up.png")

local g = anim8.newGrid(72, 26, dialogImage:getWidth(), dialogImage:getHeight())

dialogBoxAnimation = anim8.newAnimation(g('1-6',1), {[1]=0.1,[2]=0.12,[3]=0.14,[4]=0.16,[5]=0.14,[6]=0.12})

dialogElements = {}
dialogCurrentElement = 0
dialogCurrentMode = "message"
dialogAlpha = 0 -- visibility of dialog box
dialogCursor = "no" -- the choice in shop yes/no
dialogFocus = nil -- focus on one entity to prevent multiple dialogboxes

function createDialog(dialog)
	dialogElements = dialog
	dialogCurrentElement = 1
	dialogCurrentMode = dialog[1].mode
end

function updateDialog(dt)
	if player.dialogState == "active" then
		if dialogAlpha < 255 then dialogAlpha = dialogAlpha + (255 * dt * 4) end
		if dialogAlpha > 255 then dialogAlpha = 255 end
	end
	-- go through whole dialogElements and fade everyone in, wait for keypress and go to another
	-- if the element.type is shop then make another magic:
	-- display box and one draw.rectangle, keypress will change dialogCursor (rectangle will be drawn depend on this variable)
	-- the confimation will return "yes" or "no" and this will be parameter to element.shopFunction(choice)
	-- when "no" - restart all the shit and move on
	-- when "yes" - check for hearts and stuff. Display dialog for success or failure and maybe add some sparkles!!!
end

function drawDialog()
	if player.dialogState == "active" then
		local dialogBox = dialogElements[dialogCurrentElement].image
		local x = dialogElements[dialogCurrentElement].x
		local y = dialogElements[dialogCurrentElement].y
		love.graphics.setColor(255, 255, 255, dialogAlpha)
		love.graphics.draw(dialogBox, x - (dialogBox:getWidth() / 2) + 16, y - (dialogBox:getHeight()))
		love.graphics.setColor(255, 255, 255)
	end
	-- draw current dialog, maybe set variable with current dialog <3
end

function loadDialog()
	dialogMessage01	= love.graphics.newImage("gfx/dialogbox/dialogbox01.png")
	dialogMessage02	= love.graphics.newImage("gfx/dialogbox/dialogbox02.png")
	dialogMessage03	= love.graphics.newImage("gfx/dialogbox/dialogbox03.png")
	dialogMessage04	= love.graphics.newImage("gfx/dialogbox/dialogbox04.png")
	dialogMessage05	= love.graphics.newImage("gfx/dialogbox/dialogbox05.png")
	dialogMessage06	= love.graphics.newImage("gfx/dialogbox/dialogbox06.png")
	dialogMessage07	= love.graphics.newImage("gfx/dialogbox/dialogbox07.png")
end

function dialogKeypressed(key, unicode)
	if key == "o" and dialogAlpha == 255 then
		if dialogCurrentMode == "message" then
			dialogAlpha = 0
			playSound("Confirm")
			if #dialogElements > dialogCurrentElement then
				-- display next dialog box
				dialogCurrentElement = dialogCurrentElement + 1
			else
				-- hide dialog box, restart shit, go home, done
				player.dialogState = nil
				dialogElements = {}
				dialogCurrentElement = 0
				dialogFocus = nil
			end
		end
	end
end