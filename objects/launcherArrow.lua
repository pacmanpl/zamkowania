launcherArrow = {}

local launcherArrowImage = love.graphics.newImage("objects/launcherArrow.png")

local g = anim8.newGrid(30, 7, launcherArrowImage:getWidth(), launcherArrowImage:getHeight())

function launcherArrow:new(x, y, direction, arrowVelocity, arrowDamage)
	local object = {
		x = x,
		y = y,
		animFlyRight	= anim8.newAnimation(g('1-4',1), 0.05),
		animFlyLeft		= anim8.newAnimation(g('1-4',1), 0.05):flipH(),
		animDieRight	= anim8.newAnimation(g('1-4',1), 0.1),
		animDieLeft		= anim8.newAnimation(g('1-4',1), 0.1):flipH(),
		status = "alive",
		despawnTimer = 1,
		direction = direction,
		arrowVelocity = arrowVelocity,
		damage = arrowDamage,
		hitbox = nil,
		hitPlayer = false
	}
	if object.direction == 1 then
		object.launcherArrowAnimation = object.animFlyRight
	else
		object.launcherArrowAnimation = object.animFlyLeft
	end
	setmetatable( object, {__index = launcherArrow})
	return object
end

function launcherArrow:update(dt, key)
	self.launcherArrowAnimation:update(dt)
	if self.status == "dead" then self.despawnTimer = self.despawnTimer - dt end

	if self.status == "alive" then
		-- check collisions and move/kill
		local offset
		if self.direction == 1 then
			offset = 8
		else
			offset = 0
		end
		if isColliding(currentMap, self.x + offset, self.y + 2) then
			self.status = "dead"
			self.despawnTimer = 1
			if self.direction == 1 then
				self.launcherArrowAnimation = self.animDieRight
			else
				self.launcherArrowAnimation = self.animDieLeft
			end
		else
			self.x = self.x + (self.arrowVelocity * dt) * self.direction
		end

		if self.hitPlayer then
			self.status = "dead"
			self.despawnTimer = 1
			if self.direction == 1 then
				self.launcherArrowAnimation = self.animDieRight
			else
				self.launcherArrowAnimation = self.animDieLeft
			end
		end
	end

	self:hitDetection()

	-- despawning
	if self.x < camX - windowWidth
		or self.x > camX + (windowWidth * 2)
		or self.y < camY - windowHeight
		or self.y > camY + (windowHeight * 2) then

		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		entities[key] = nil
	end
end

function launcherArrow:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x
	box[2] = self.y + 1
	box[3] = self.x + 28
	box[4] = self.y + 5

	self.hitbox = {}
	self.hitbox[1] = box

	if (self.hitbox[1][1] <= player.hitbox[3]-2 and self.hitbox[1][3] >= player.hitbox[1]+2 and
		self.hitbox[1][2] <= player.hitbox[4]-2 and self.hitbox[1][4] >= player.hitbox[2]+2) then
	
		-- kill arrow when it touch player
		if self.status == "alive" then
			self.hitPlayer = true
		end
	end
end

function launcherArrow:draw()
	self.launcherArrowAnimation:draw(launcherArrowImage, math.floor(self.x), math.floor(self.y))
	--love.graphics.rectangle("fill", self.hitbox[1], self.hitbox[2], 12, 12)
end

function launcherArrow:keypressed()
end