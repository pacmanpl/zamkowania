meatBall = {}

local meatBallImage = love.graphics.newImage("objects/meat-ball.png")

local g = anim8.newGrid(32, 32, meatBallImage:getWidth(), meatBallImage:getHeight())

function meatBall:new(x, y, xVelocity, yVelocity, gravity, damage)
	local object = {
		x = x,
		y = y,
		xVelocity = xVelocity,
		yVelocity = yVelocity,
		gravity = gravity,
		width = 32,
		height = 32,
		animFly	= anim8.newAnimation(g('1-6',1), 0.1),
		status = "alive",
		rotation = 0,

		damage = damage,		-- damage dealt on hit
		hitbox = nil,
	}
	object.meatBallAnimation = object.animFly
	setmetatable( object, {__index = meatBall})
	return object
end

function meatBall:update(dt, key)

	self.yVelocity = self.yVelocity + (self.gravity * dt)
	self.x = self.x + (self.xVelocity * dt)
	self.y = self.y + (self.yVelocity * dt)

	self.rotation = self.rotation + (15 * dt)

	self:hitDetection()

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 64
		or self.x > camX + windowWidth + 64
		or self.y < camY - 64
		or self.y > camY + windowHeight + 64 then

		entities[key] = nil
	end
end

function meatBall:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 5
	box[2] = self.y - 5
	box[3] = self.x + 6
	box[4] = self.y + 6

	self.hitbox = {}
	self.hitbox[1] = box

	--if (self.hitbox[1] <= player.hitbox[3] and self.hitbox[3] >= player.hitbox[1] and
	--	self.hitbox[2] <= player.hitbox[4] and self.hitbox[4] >= player.hitbox[2]) then
	--
	--	-- despawn meatBall when it hits player
	--	entities[key] = nil
	--end
end

function meatBall:draw()
	self.meatBallAnimation:draw(meatBallImage, math.floor(self.x), math.floor(self.y), self.rotation, 1, 1, 16, 16)
	--love.graphics.rectangle("fill", self.hitbox[1], self.hitbox[2], 12, 12)
end

function meatBall:keypressed()
end