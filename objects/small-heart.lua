smallHeart = {}

local smallHeartImage = love.graphics.newImage("gfx/small-hearth.png")

local g = anim8.newGrid(50, 50, smallHeartImage:getWidth(), smallHeartImage:getHeight())

function smallHeart:new(x, y)
	local object = {
		x = x,
		y = y,
		state = "spawning", -- spawning/despawning/floating
		timerSpawning = 0.89,
		timerDeSpawning = 0.9,
		timerPosition = 0.15,
		direction = 1, -- 1 means up negative 1 means down
		yOffset = 0,
		fallingSpeed = 20,
		animSmallHeartSpawn		= anim8.newAnimation(g('1-12',1), 0.08, 'pauseAtEnd'),
		animSmallHeartFloat		= anim8.newAnimation(g('1-10',2), 0.1),
		animSmallHeartDeSpawn	= anim8.newAnimation(g('1-14',3), 0.08, 'pauseAtEnd'),
	}
	object.smallHeartAnimation = object.animSmallHeartSpawn
	setmetatable( object, {__index = smallHeart})
	return object
end

function smallHeart:update(dt, key)
	-- swaying animation
	self.smallHeartAnimation:update(dt)
	if self.state == "spawning" then
		self.timerSpawning = self.timerSpawning - dt
		if self.timerSpawning <= 0 then
			self.state = "floating"
			self.smallHeartAnimation = self.animSmallHeartFloat
		end
	end
	if self.state == "floating" then
		self.timerPosition = self.timerPosition - dt

		if self.timerPosition <= 0 then
			if self.yOffset ~= 4 and self.yOffset ~= -4 then
				self.yOffset = self.yOffset + self.direction
			end
			if self.yOffset == 3 then
				self.direction = -1
			elseif self.yOffset == -3 then
				self.direction = 1
			end

			-- reset timerPosition
			self.timerPosition = 0.15 + (self.yOffset * -0.01)
		end

		-- falling if it's in air
		if not isColliding(currentMap, self.x, self.y + 30) then
			self.y = self.y + (self.fallingSpeed * dt)
			self.fallingSpeed = self.fallingSpeed + (500 * dt)
		end
	end
	if self.state == "despawning" then
		self.timerDeSpawning = self.timerDeSpawning - dt
		if self.timerDeSpawning <= 0 then
			-- delete entity
			entities[key] = nil
		end
	end

	-- check if player grabed heart
	if 	player.x - self.x <= 16 and
		player.x - self.x >= -16 and
		player.y - self.y <= 25 and
		player.y - self.y >= -25 and
		self.state == "floating" then
		-- despawn animation, +++ heart
		self.smallHeartAnimation = self.animSmallHeartDeSpawn
		self.state = "despawning"

		player.hearts = player.hearts + 1
	end
	-- check if player is far away
	if self.x < camX - 640
		or self.x > camX + windowWidth + 480
		or self.y < camY - 480
		or self.y > camY + windowHeight + 480 then

		entities[key] = nil
	end
end

function smallHeart:draw()
	-- draw animation minus 25 pixels - half of the frame
	self.smallHeartAnimation:draw(smallHeartImage, math.floor(self.x - 25), math.floor(self.y - 25) + self.yOffset)
end

function smallHeart:keypressed(key, unicode)

end