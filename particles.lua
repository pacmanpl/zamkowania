Particle = {}

function Particle:new(name, x, y, size, soulCount)
	local object = {
		name = name,
		x = x,
		y = y,
		soulCount = soulCount
	}
	--[[

	Soul powinien się snapować od początku... może
	Zmienić wielkość souli

	]]
	if object.name == "soul" then
		object.p = 							love.graphics.newParticleSystem(particleImage01, 200)
		object.p:setEmissionRate			(44)
 		object.p:setEmitterLifetime			(-1)
 		object.p:setParticleLifetime		(5)
 		object.p:setPosition				(0, 0)
 		object.p:setDirection				(0)
 		object.p:setSpeed					(1, 1)
 		object.p:setRadialAcceleration		(-5)
 		object.p:setSizes					(0.5, 1, 2)
 		object.p:setSizeVariation			(0.4)
 		object.p:setColors					(89, 124, 15, 240, 203, 245, 114, 240, 251, 175, 93, 200, 155, 86, 12, 20)
 		object.p:setTangentialAcceleration	(0,0)
 		object.p:setRelativeRotation		(true)
 		object.p:setAreaSpread				('normal', size, size)
 		object.p:start()
 		object.state = "born"
 		object.bornTimer = 3
 		object.grabed    = false
 		object.xVelocity = 1
 		object.yVelocity = 1
	end
	setmetatable( object, {__index = Particle})
	return object
end

function Particle:update(dt, key)
	self.p:update(dt)
	if self.name == "soul" then
		if self.bornTimer > 0 then self.bornTimer = self.bornTimer - dt end

		if self.state == "born" then
			self.p:setParticleLifetime (math.clamp(4 * (math.abs(player.x - self.x) / 150), 1, 4))

			local angle = math.atan2((player.y - self.y), (player.x - self.x))

			self.xVelocity = 30 * math.cos(angle)
			self.yVelocity = 30 * math.sin(angle)

			self.x = self.x + (self.xVelocity * dt)

			self.y = self.y + (self.yVelocity * dt)

			if self.bornTimer <= 0 then
				self.state = "snap"
			end
		end

		if self.state == "snap" then
			local angle = math.atan2((player.y - self.y), (player.x - self.x))
			local speedX, speedY

			speedX = 25 * (math.clamp(math.abs((player.x - self.x) / 20), 0, 1)) * math.cos(angle)
			speedY = 25 * (math.clamp(math.abs((player.y - self.y) / 20), 0, 1)) * math.sin(angle)
			self.p:setLinearAcceleration		(speedX, speedY, speedX, speedY)

			self.p:setRadialAcceleration		(0)
			self.p:setAreaSpread				('normal', 5, 5)
			self.p:setParticleLifetime			(1.5)

			local d = 135
			if self.x < player.x - 90 or self.x > player.x + 90 then
				d = d * 2
			else
				d = 135
			end
			self.xVelocity = self.xVelocity + (d * math.cos(angle) * dt)
			self.yVelocity = 155 * math.sin(angle)

			self.xVelocity = math.clamp(self.xVelocity, -140, 140)
			self.x = self.x + (self.xVelocity * dt)

			self.yVelocity = math.clamp(self.yVelocity, -120, 120)
			self.y = self.y + (self.yVelocity * dt)

			if self.x > player.x - 15 and self.x < player.x + 15 and
			   self.y > player.y - 15 and self.y < player.y + 15 then
			   self.state = "die"
			end
		end

		if self.state == "die" then
			self.p:stop()
			if self.grabed == false then
				--player.soulTimer = 4
				player.p1:start()
 				player.p1:setEmitterLifetime       (1)
				playSound("CollectSoul")
				if (player.soulBufferTimer <= 0 and player.soulBuffer <= 0) or player.soulBufferTimer > 0 then
					player.soulBufferTimer = 1
				end
				player.soulBuffer = player.soulBuffer + self.soulCount
				self.grabed = true
			end
			self.x = self.x + ((player.x - self.x) * dt * 2)
			self.y = self.y + ((player.y - self.y) * dt * 2)
		end
	end
end

function Particle:draw()
	love.graphics.setBlendMode("additive")
	love.graphics.draw(self.p, self.x, self.y)
	love.graphics.setBlendMode("alpha")
end