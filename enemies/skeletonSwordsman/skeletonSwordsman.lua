SkeletonSwordsman = {}

local skeletonSwordsmanImage  = love.graphics.newImage("enemies/skeletonSwordsman/sprite.png")
local skeletonSwordsmanImage2 = love.graphics.newImage("enemies/skeletonSwordsman/death.png")

local g = anim8.newGrid(168, 83, skeletonSwordsmanImage:getWidth(), skeletonSwordsmanImage:getHeight())
local k = anim8.newGrid(60, 83, skeletonSwordsmanImage2:getWidth(), skeletonSwordsmanImage2:getHeight())

function SkeletonSwordsman:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		xVelocity = 0,
		width = 34,
		height = 83,
		walkSpeed = 70,
		attackSpeed = 210,
		attackFriction = 280,
		direction = "none",
		spawnNumber = spawnID,
		animWalkRight	= anim8.newAnimation(g('1-2',1), 0.25):flipH(),
		animWalkLeft	= anim8.newAnimation(g('1-2',1), 0.25),
		animChargeRight	= anim8.newAnimation(g(3,1), 0.1):flipH(),
		animChargeLeft	= anim8.newAnimation(g(3,1), 0.1),
		animAttackRight	= anim8.newAnimation(g(4,1), 1.0):flipH(),
		animAttackLeft	= anim8.newAnimation(g(4,1), 1.0),
		animDieRight	= anim8.newAnimation(k('1-13',1), 0.09):flipH(),
		animDieLeft		= anim8.newAnimation(k('1-13',1), 0.09),
		state = "step",
		stepCount = 0,
		keepDistance = 130,
		patternTimer = 0,
		dontMove = false,
		debug = "",
		didntHaveBreak = true,

		damage = 1,
		hitbox = nil,
		status = "alive",
		despawnTimer = 1.3,
		stunTime = 0,			-- how much stun is left
		stunDuration = 0.15,	-- how much time entity will be stuned
		invulnerableTimer = 0,	-- how much time entity will be invulnerable
		health = 4
	}
	object.skeletonSwordsmanAnimation = object.animWalkLeft
	setmetatable( object, {__index = SkeletonSwordsman})
	return object
end

function SkeletonSwordsman:update(dt, key)
	-- update animation
	if self.stunTime <= 0 then self.skeletonSwordsmanAnimation:update(dt) end
	-- take off dt from stun time
	if self.stunTime > 0 then self.stunTime = self.stunTime - dt end
	-- update timer for death animation
	if self.status == "dead" and self.despawnTimer > 0 then self.despawnTimer = self.despawnTimer - dt end
	-- update invulnerable time
	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	-- update pattern timer
	if self.patternTimer > 0 then self.patternTimer = self.patternTimer - dt end

	if self.status == "alive" and self.stunTime <= 0 then

		-- update direction when is not attacking
		if self.state ~= "attacking" then
			local playerPosition = player.x - self.x
			if playerPosition <= 0 then
				self.direction = "left"
			else
				self.direction = "right"
			end
		end

		-- if there is no steps to step, try attack if not step some more
		if self.stepCount == 0 and self.patternTimer <= 0 and self.state == "step" then
			-- if player is in attack range charge
			self.debug = "czekin for playa"
			if (self.x - player.x > -self.keepDistance and self.x - player.x < self.keepDistance) and
			    (player.y > self.y + 0 and player.y < self.y + 67) then

				self.state = "charge"
				self.patternTimer = 0.4
			else
				self.debug = "random steps"
				-- if not random step count
				self.stepCount = math.random(1, 4)
				self.state = "step"
			end
		end

		-- try to take a break?
		if self.patternTimer <= 0 and self.didntHaveBreak and self.state == "step" then
			-- make a break
			self.state = "break"
			self.xVelocity = 0
			self.patternTimer = 0.2
			self.didntHaveBreak = false
		end

		if self.state == "break" and self.patternTimer <= 0 then
			self.state = "step"
			self.didntHaveBreak = true
		end

		if self.patternTimer <= 0 and self.state == "step" and self.stepCount > 0 then
			self.debug = "trying to step"
			-- decrease stepCount
			self.stepCount = self.stepCount - 1
			-- stop for a moment
			self.xVelocity = 0
			-- step step, keep the distance!
			local time = math.random(18, 30)
			time = time / 100
			self.patternTimer = time

			-- if player is in the distance back off
			local distance = player.x - self.x
			if distance > -self.keepDistance and distance < self.keepDistance then
				if distance > 0 then
					self.xVelocity = -self.walkSpeed
				else
					self.xVelocity = self.walkSpeed
				end
			else
				if distance > 0 then
					self.xVelocity = self.walkSpeed
				else
					self.xVelocity = -self.walkSpeed
				end
			end

			if self.direction == "left" then
				self.skeletonSwordsmanAnimation = self.animWalkLeft
			else
				self.skeletonSwordsmanAnimation = self.animWalkRight
			end
		end

		if self.state == "charge" then
			self.xVelocity = 0

			if self.direction == "left" then
				self.skeletonSwordsmanAnimation = self.animChargeLeft
			else
				self.skeletonSwordsmanAnimation = self.animChargeRight
			end

			if self.patternTimer <= 0 then
				self.state = "attack"
				self.patternTimer = 0.8
			end
		end

		if self.state == "attack" then
			if self.direction == "left" then
				self.xVelocity = -self.attackSpeed
				self.skeletonSwordsmanAnimation = self.animAttackLeft
			else
				self.xVelocity =  self.attackSpeed
				self.skeletonSwordsmanAnimation = self.animAttackRight
			end

			self.state = "attacking"
		end

		if self.state == "attacking" then
			if self.patternTimer <= 0 then
				self.state = "step"
				self.stepCount = 1
			end
		end

		-- check for collision in front of skeletonSwordsman
		local offset
		if self.xVelocity < 0 then offset = -18 else
			offset = 18
		end

		if isColliding(currentMap, self.x + offset, self.y + self.height - 1) then
			self.dontMove = true
			self.debug = "dontMove!"
		else -- when no obstacles check if there is a floor
			if isColliding(currentMap, self.x + offset, self.y + self.height) then
				self.dontMove = false
				self.debug = "    Move!"
			else
				self.dontMove = true
				self.debug = "dontMove!"
			end
		end

	if not self.dontMove then
		if self.state == "attacking" then
			local d
			if self.direction == "left" then
				d = 1
			else
				d = -1
			end
			self.xVelocity = self.xVelocity + (self.attackFriction * dt * d)
			self.x = self.x + (self.xVelocity * dt)
		else
			self.x = self.x + (self.xVelocity * dt)
		end
	end
	end -- /if status == alive

	self:hitDetection()

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 480
		or self.x > camX + windowWidth + 480
		or self.y < camY - 480
		or self.y > camY + windowHeight + 480 then

		spawns[self.spawnNumber].status = "pending"
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- drop heart
		--local rand = math.random(1,100)
		--if rand <= 100 then
		--	table.insert( entities, smallHeart:new(self.x, self.y + 16))
		--end
		-- change spawn state to pending
		self.xVelocity = 0
		spawns[self.spawnNumber].status = "pending"
		-- remove entity
		entities[key] = nil
	end
end

function SkeletonSwordsman:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 8
	box[2] = self.y + 25
	box[3] = self.x + 8
	box[4] = self.y + 64

	self.hitbox = {}
	self.hitbox[1] = box

	-- sword hitbox x+73
	-- add sword hitbox when attacking
	if self.state == "attacking" then
		local box = {}

		if self.direction == "left" then
			box[1] = self.x - 72
			box[3] = self.x
		else
			box[1] = self.x
			box[3] = self.x + 72
		end

		box[2] = self.y + 40
		box[4] = self.y + 46

		self.hitbox[2] = box
	end

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.4
			self.health = self.health - player.whipDamage
			self.stunTime = self.stunDuration
			-- check if skeletonSwordsman is 'dead'
			if self.health <= 0 then
				-- status = dead
				-- if despawnTimer <= 0 then removeEntity = true
				-- play sound when timer is full
				-- start animation
				self.status = "dead"
				if self.direction == "right" then
					self.skeletonSwordsmanAnimation = self.animDieRight
				else
					self.skeletonSwordsmanAnimation = self.animDieLeft
				end
				if self.despawnTimer == 1.3 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 12, 20))
				end
			end
		end
	end
end

function SkeletonSwordsman:draw()
	if self.status == "alive" then
		if self.invulnerableTimer > 0 then
			love.graphics.setColor( math.random(10,255), math.random(10,255), math.random(10,255) )
			self.skeletonSwordsmanAnimation:draw(skeletonSwordsmanImage, math.floor(self.x) - (168 / 2), math.floor(self.y))
			love.graphics.setColor(255, 255, 255)
		else
			self.skeletonSwordsmanAnimation:draw(skeletonSwordsmanImage, math.floor(self.x) - (168 / 2), math.floor(self.y))
		end
	else
		self.skeletonSwordsmanAnimation:draw(skeletonSwordsmanImage2, math.floor(self.x) - (60 / 2), math.floor(self.y))
	end
end

function SkeletonSwordsman:keypressed()
end