Skeleton = {}

local skeletonImage = love.graphics.newImage("enemies/skeleton/sprite.png")
local skeletonImage2 = love.graphics.newImage("enemies/skeleton/death.png")

local g = anim8.newGrid(32, 64, skeletonImage:getWidth(), skeletonImage:getHeight())
local k = anim8.newGrid(54, 64, skeletonImage2:getWidth(), skeletonImage2:getHeight())

function Skeleton:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		width = 32,
		height = 64,
		walkSpeed = 35,
		direction = "none",
		spawnNumber = spawnID,
		animSkeletonDieRight	= anim8.newAnimation(k('1-21',1), {['1-4']=0.1,['5-8']=0.08,[9]=0.2,['10-12']=0.05,['13-15']=0.08,['16-18']=0.09,['19-21']=0.09}, 'pauseAtEnd'):flipH(),
		animSkeletonDieLeft		= anim8.newAnimation(k('1-21',1), {['1-4']=0.1,['5-8']=0.08,[9]=0.2,['10-12']=0.05,['13-15']=0.08,['16-18']=0.09,['19-21']=0.09}, 'pauseAtEnd'),
		animSkeletonWalkRight	= anim8.newAnimation(g('1-7',1), {[1]=0.1,['2-4']=0.13,['5-7']=0.2}, 'pauseAtEnd'):flipH(),
		animSkeletonWalkLeft	= anim8.newAnimation(g('1-7',1), {[1]=0.1,['2-4']=0.13,['5-7']=0.2}, 'pauseAtEnd'),
		patters = "idle", -- idle, step
		timer = 1.13,		 -- timer for walking animation
		despawnTimer = 1.86, -- timer for death animation, after139

		damage = 1,		-- damage dealt on hit
		hitbox = nil,
		status = "alive",
		stunTime = 0,			-- how much stun is left
		stunDuration = 0.15,	-- how much time entity will be stuned
		invulnerableTimer = 0,	-- how much time entity will be invulnerable
		health = 2
	}
	object.skeletonAnimation = object.animSkeletonWalkLeft
	setmetatable( object, {__index = Skeleton})
	return object
end

function Skeleton:update(dt, key)
	-- update animation
	if self.stunTime <= 0 then self.skeletonAnimation:update(dt) end
	-- take off dt from stun time
	if self.stunTime > 0 then self.stunTime = self.stunTime - dt end
	if self.stunTime < 0 then self.stunTime = 0 end
	-- update timer for movement
	if self.stunTime == 0 then self.timer = self.timer - dt end
	-- update timer for death animation
	if self.status == "dead" and self.despawnTimer > 0 then self.despawnTimer = self.despawnTimer - dt end
	-- update invulnerable time
	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	-- set starting direction and animation
	if self.direction == "none" then
		local playerPosition = player.x - self.x
		if playerPosition <= 0 then
			self.direction = "left"
		else
			self.direction = "right"
		end
	end
	if self.status == "alive" then
		if self.direction == "right" then
				self.skeletonAnimation = self.animSkeletonWalkRight
		else
				self.skeletonAnimation = self.animSkeletonWalkLeft
		end

		-- check for collision in front of skeleton
		local offset
		if self.direction == "left" then offset = -13 else
			offset = 13
		end

		if isColliding(currentMap, self.x + offset, self.y + self.height - 1) then
			-- turn around
			if self.direction == "right" then
				self.direction = "left"
				self.timer = 1.09
				self.animSkeletonWalkLeft:gotoFrame(1)
			elseif self.direction == "left" then
				self.direction = "right"
				self.timer = 1.09
				self.animSkeletonWalkRight:gotoFrame(1)
			end
		else -- when no obstacles check if there is a floor
			if isColliding(currentMap, self.x + offset, self.y + self.height) then
				if self.timer <= 1.09 and self.timer > 0.2 and self.stunTime == 0 then
					if self.direction == "right" then
						self.x = self.x + (self.walkSpeed * dt)
					else
						self.x = self.x + (self.walkSpeed * dt) * -1
					end
				end
			else
				-- turn around
				if self.direction == "right" then
					self.direction = "left"
					self.timer = 1.09
					self.animSkeletonWalkLeft:gotoFrame(1)
				elseif self.direction == "left" then
					self.direction = "right"
					self.timer = 1.09
					self.animSkeletonWalkRight:gotoFrame(1)
				end
			end
			
		end
		if self.timer <= 0 then
			self.timer = 1.09
			self.animSkeletonWalkLeft:gotoFrame(1)
			self.animSkeletonWalkLeft.status = "playing"
			self.animSkeletonWalkRight:gotoFrame(1)
			self.animSkeletonWalkRight.status = "playing"
		end
	end -- /if status == alive

	self:hitDetection()

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 640
		or self.x > camX + windowWidth + 480
		or self.y < camY - 480
		or self.y > camY + windowHeight + 480 then

		spawns[self.spawnNumber].status = "pending"
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- drop heart
		--local rand = math.random(1,100)
		--if rand <= 100 then
		--	table.insert( entities, smallHeart:new(self.x, self.y + 16))
		--end

		self.animSkeletonDieRight:gotoFrame(1)
		self.animSkeletonDieRight.status = "playing"
		self.animSkeletonDieLeft:gotoFrame(1)
		self.animSkeletonDieLeft.status = "playing"
		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		-- remove entity
		entities[key] = nil
	end
end

function Skeleton:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 8
	box[2] = self.y + 15
	box[3] = self.x + 8
	box[4] = self.y + 64

	local box2 = {}
	box2[1] = self.x - 8
	box2[2] = self.y + 15
	box2[3] = self.x + 8
	box2[4] = self.y + 64

	self.hitbox = {}
	self.hitbox[1] = box
	self.hitbox[2] = box2

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.4
			self.health = self.health - player.whipDamage
			self.stunTime = self.stunDuration
			-- check if skeleton is 'dead'
			if self.health <= 0 then
				-- status = dead
				-- if despawnTimer <= 0 then removeEntity = true
				-- play sound when timer is full
				-- start animation
				self.status = "dead"
				if self.direction == "right" then
					self.skeletonAnimation = self.animSkeletonDieRight
				else
					self.skeletonAnimation = self.animSkeletonDieLeft
				end
				if self.despawnTimer == 1.86 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 9, 10))
				end
			end
		end
	end
end

function Skeleton:draw()
	if self.status == "alive" then
		if self.invulnerableTimer > 0 then
			love.graphics.setColor( math.random(10,255), math.random(10,255), math.random(10,255) )
			self.skeletonAnimation:draw(skeletonImage, math.floor(self.x) - (32 / 2), math.floor(self.y))
			love.graphics.setColor(255, 255, 255)
		else
			self.skeletonAnimation:draw(skeletonImage, math.floor(self.x) - (32 / 2), math.floor(self.y))
		end
	else
		if self.direction == "left" then
			self.skeletonAnimation:draw(skeletonImage2, math.floor(self.x) - (54 / 2) + 9, math.floor(self.y))
		else
			self.skeletonAnimation:draw(skeletonImage2, math.floor(self.x) - (54 / 2) - 9, math.floor(self.y))
		end
	end
	--love.graphics.print(self.despawnTimer, camX + 146, camY + 60)
	--love.graphics.print(self.health, camX + 176, camY + 45)
	--love.graphics.rectangle("fill", self.hitbox[1], self.hitbox[2], 16, 49)
end

function Skeleton:keypressed()
end