Bat = {}

local batImage = love.graphics.newImage("enemies/bat/sprite.png")
local batImage2 = love.graphics.newImage("enemies/bat/death.png")

local g = anim8.newGrid( 18, 20, batImage:getWidth(), batImage:getHeight())
local k = anim8.newGrid( 18, 20, batImage2:getWidth(), batImage2:getHeight())

function Bat:new(spawnID, x, y)
	object = {
		x = x,
		originalY = y,
		y = y,
		width = 18,
		height = 20,
		xVelocity = math.random(80, 120),
		yVelocity = -60,-- _\
						-- _ \	Same :S
		jumpSpeed = -60,--  /
		jumpDirection = -1,
		jumpGravity = 200,
		direction = 0,
		spawnNumber = spawnID,

		status = "alive",
		health = 1,
		damage = 1,
		hitbox = nil,
		despawnTimer = 0.5,

		animFlyLeft		= anim8.newAnimation(g('1-3',1), 0.07),
		animFlyRight	= anim8.newAnimation(g('1-3',1), 0.07):flipH(),
		animDie			= anim8.newAnimation(k('1-8',1), 0.05, 'pauseAtEnd')
	}
	object.batAnimation = object.animFlyLeft
	setmetatable( object, {__index = Bat})
	return object
end

function Bat:update(dt, key)
	self.batAnimation:update(dt)
	if self.status == "dead" then self.despawnTimer = self.despawnTimer - dt end

	-- start with facing the player
	if self.direction == 0 then
		local direction = self.x - player.x
		if direction > 0 then
			self.direction = -1
			self.batAnimation = self.animFlyLeft
		else
			self.direction = 1
			self.batAnimation = self.animFlyRight
		end
	end

	if self.status == "alive" then
		-- FLY x-axis!
		self.x = self.x + (self.xVelocity * dt * self.direction)

		-- FLY y-axis!
		self.yVelocity = self.yVelocity + (self.jumpGravity * dt * -self.jumpDirection)

		self.y = self.y + (self.yVelocity * dt)

		if self.y >= self.originalY and self.jumpDirection == -1 then
			self.jumpDirection = self.jumpDirection * -1
			self.yVelocity = self.jumpSpeed * -1
		elseif self.y <= self.originalY and self.jumpDirection == 1 then
			self.jumpDirection = self.jumpDirection * -1
			self.yVelocity = self.jumpSpeed
		end
		self:hitDetection()
	end -- /if status == alive

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 640
		or self.x > camX + windowWidth + 480
		or self.y < camY - 480
		or self.y > camY + windowHeight + 480 then

		spawns[self.spawnNumber].status = "pending"
		spawns[self.spawnNumber].count = spawns[self.spawnNumber].count - 1
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- not droping hearts
		self.animDie:gotoFrame(1)
		self.animDie.status = "playing"
		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		spawns[self.spawnNumber].count = spawns[self.spawnNumber].count - 1
		-- remove entity
		entities[key] = nil
	end
end

function Bat:hitDetection()
	-- update hitbox
	local hitbox = {}
	hitbox.x1 = self.x - 9
	hitbox.y1 = self.y + 10
	hitbox.x2 = self.x + 9
	hitbox.y2 = self.y + 18
	self.hitbox = {hitbox.x1, hitbox.y1, hitbox.x2, hitbox.y2}

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil then
		if (player.whipHitbox[1] <= self.hitbox[3] and player.whipHitbox[3] >= self.hitbox[1] and
			player.whipHitbox[2] <= self.hitbox[4] and player.whipHitbox[4] >= self.hitbox[2]) and
			self.status == "alive" then
			love.audio.play(sfxWhipHit)
			self.health = self.health - player.whipDamage
			-- check if bat is 'dead'
			if self.health <= 0 then
				self.status = "dead"
				self.batAnimation = self.animDie
				if self.despawnTimer == 0.5 then
					love.audio.play(sfxEnemyDeath)
				end
			end
		end
	end
end

function Bat:draw()
	if self.status == "alive" then
		self.batAnimation:draw(batImage, math.floor(self.x) - 9, math.floor(self.y) + 10)
	else
		self.batAnimation:draw(batImage2, math.floor(self.x) - 9, math.floor(self.y) + 10)
	end


	--love.graphics.rectangle("fill", self.x - 9, self.y + 10, 18, 12)
end

function Bat:keypressed(key, unicode)
end