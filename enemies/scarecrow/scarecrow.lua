Scarecrow = {}

local scarecrowImage = love.graphics.newImage("enemies/scarecrow/sprite.png")

local g = anim8.newGrid( 50, 75, scarecrowImage:getWidth(), scarecrowImage:getHeight())

function Scarecrow:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		width = 50,
		height = 75,
		xVelocity = 0,
		yVelocity = 0,
		yMaxVelocity = 750,
		gravity = 750,
		spawnNumber = spawnID,
		direction = "none",
		state = "stand",
		onFloor = true,
		isJumping = false,
		jumpSpeed = 0,
		rotation = 0,
		rotateDirection = 0,
		rotateAmp = 0,

		animStand	= anim8.newAnimation(g(1,1), 1),
		animDie		= anim8.newAnimation(g('1-14',2), 0.08, 'pauseAtEnd'),

		status = "alive",
		health = 5,
		damage = 1,
		hitbox = nil,
		despawnTimer = 1.4,
		stunTime = 0,
		stunDuration = 0.25,
		invulnerableTimer = 0
	}
	object.scarecrowAnimation = object.animStand
	setmetatable( object, {__index = Scarecrow})
	return object
end

function Scarecrow:update(dt, key)
	-- timers
	if self.stunTime <= 0 then self.scarecrowAnimation:update(dt) end
	if self.stunTime > 0 then self.stunTime = self.stunTime - dt end
	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	if self.status == "dead" then self.despawnTimer = self.despawnTimer - dt end

	if self.status == "alive" and self.stunTime <= 0 then
		-- before player goes close to scarecrow
		if self.state == "stand" then
			if (-8 * 16) < (self.x - player.x) and (self.x - player.x) < (8 * 16) then
				self.state = "jump"
			end
		end

		if self.state == "jump" then
			if self.onFloor then
				local jumpDirection, walkspeed
				if (self.x - player.x) > 0 then jumpDirection = -1
					else jumpDirection = 1 end

				local jumpRandomizer = math.random(1, 10)
				if jumpRandomizer <= 5 then
					self.jumpSpeed = -math.random(100, 135)
					walkspeed = 150
				else
					self.jumpSpeed = -math.random(300, 345)
					walkspeed = 150
				end

				-- send this motherfucker in the air
				self.yVelocity	= self.jumpSpeed
				self.xVelocity	= walkspeed * jumpDirection
				self.onFloor	= false
				self.isJumping	= true
				self.rotateDirection = (math.random(1, 2) * 2) - 3
			end -- /if onFloor

			if self.isJumping then
				self.yVelocity = self.yVelocity + (self.gravity * dt)

				local nextX = self.x + (self.xVelocity * dt)
				if self.xVelocity < 0 then
					if	not isColliding( currentMap, nextX - 25 - 11, self.y + 59) then
						self.x = nextX
					else
						self.x = self.x
					end
				elseif self.xVelocity > 0 then
					if	not isColliding( currentMap, nextX - 25 + 10, self.y + 59) then
						self.x = nextX
					else
						local wallGap = (math.ceil(self.x / 16) * 16) - self.x
						self.x = self.x + wallGap - 1
					end
				end

				local nextY = self.y + (self.yVelocity * dt)
				if self.yVelocity > 0 then
					if	not isColliding( currentMap, self.x - 25 +  9, nextY + 75) and
						not isColliding( currentMap, self.x - 25 - 11, nextY + 75) and
						not isColliding( currentMap, self.x - 25	 , nextY + 75) then
						self.y = nextY
						if self.rotation > 0 then
							self.rotation = self.rotation - (2.5 * dt)
						end
						if self.rotation < 0 then
							self.rotation = self.rotation + (2.5 * dt)
						end
					else
						-- hit the floor
						self.yVelocity = 0
						self.xVelocity = 0
						self.isJumping = false
						self.onFloor = true
						self.rotateAmp = 0
					end
				elseif self.yVelocity < 0 then
					self.y = nextY
					--self.rotation = self.rotation + (self.rotateAmp * self.rotateDirection)
					self.rotation = self.rotation + (1.5 * dt) * self.rotateDirection
				end
			end
		end
		self:hitDetection()
	end -- /if status = alive

	if self.status == "dead" then
		self.rotation = 0
		self.x = self.x + (self.xVelocity * dt)
		self.y = self.y - (self.yVelocity * dt)
		self.yVelocity = self.yVelocity - (gravity * dt)
	end

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 640
		or self.x > camX + windowWidth + 480
		or self.y < camY - 480
		or self.y > camY + windowHeight + 480 then

		spawns[self.spawnNumber].status = "pending"
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- not droping hearts
		self.animDie:gotoFrame(1)
		self.animDie.status = "playing"
		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		-- remove entity
		entities[key] = nil
	end
end

function Scarecrow:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 6 - 25
	box[2] = self.y
	box[3] = self.x + 6 - 25
	box[4] = self.y + 50

	self.hitbox = {}
	self.hitbox[1] = box

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.5
			self.health = self.health - player.whipDamage
			self.stunTime = self.stunDuration
			-- check if scarecrow is 'dead'
			if self.health <= 0 then
				local direction
				if player.direction == "right" then
					direction =  1
				else
					direction = -1
				end
				self.xVelocity = 110 * direction
				self.yVelocity = 200
				self.isJumping = true
				self.onFloor   = false

				self.status = "dead"
				self.scarecrowAnimation = self.animDie
				if self.despawnTimer == 1.4 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 12, 20))
				end
			end
		end
	end
end

function Scarecrow:draw()
	if self.status == "alive" then
		if self.invulnerableTimer > 0 then
			love.graphics.setColor( math.random(10,255), math.random(10,255), math.random(10,255) )
			self.scarecrowAnimation:draw(scarecrowImage, math.floor(self.x) - 25, math.floor(self.y) + 37, self.rotation, 1, 1, 25, 37)
			love.graphics.setColor(255, 255, 255)
		else
			self.scarecrowAnimation:draw(scarecrowImage, math.floor(self.x) - 25, math.floor(self.y) + 37, self.rotation, 1, 1, 25, 37)
		end
	else
		self.scarecrowAnimation:draw(scarecrowImage, math.floor(self.x) - 25, math.floor(self.y) + 37, self.rotation, 1, 1, 25, 37)
	end
end

function Scarecrow:keypressed(key, unicode)
end