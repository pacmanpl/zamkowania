Wolf = {}

local wolfImage  = love.graphics.newImage("enemies/wolf/sprite.png")
local wolfImage2 = love.graphics.newImage("enemies/wolf/spriteAttack.png")
local wolfImage3 = love.graphics.newImage("enemies/wolf/spriteDeath.png")

local g = anim8.newGrid(69, 40, wolfImage:getWidth(), wolfImage:getHeight())
local k = anim8.newGrid(85, 40, wolfImage2:getWidth(), wolfImage2:getHeight())
local l = anim8.newGrid(69, 40, wolfImage3:getWidth(), wolfImage3:getHeight())

function Wolf:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		width = 78,
		height = 40,
		walkspeed = 30,
		state = "walk",
		direction = "right",
		spawnNumber = spawnID,
		animStandLeft		= anim8.newAnimation(g(1,1), 0.1),
		animStandRight		= anim8.newAnimation(g(1,1), 0.1):flipH(),
		animCooldownLeft	= anim8.newAnimation(g('1-13',2), 0.1, 'pauseAtEnd'),
		animCooldownRight	= anim8.newAnimation(g('1-13',2), 0.1, 'pauseAtEnd'):flipH(),
		animWalkLeft 		= anim8.newAnimation(g('2-15',1), 0.1),
		animWalkRight		= anim8.newAnimation(g('2-15',1), 0.1):flipH(),
		animAttackLeft		= anim8.newAnimation(k('1-3',1), 0.08, 'pauseAtEnd'),
		animAttackRight		= anim8.newAnimation(k('1-3',1), 0.08, 'pauseAtEnd'):flipH(),
		animDieLeft			= anim8.newAnimation(l('1-28',1), 0.08, 'pauseAtEnd'),
		animDieRight		= anim8.newAnimation(l('1-28',1), 0.08, 'pauseAtEnd'):flipH(),
		patternTimer = 0,
		attackDestination = 0, --128
		despawnTimer = 2.4,

		damage = 1,
		hitbox = nil,
		status = "alive",
		isAttacking = false,
		stunTime = 0,
		stunDuration = 0.15,
		invulnerableTimer = 0,
		health = 1
	}
	object.wolfAnimation = object.animWalkLeft
	setmetatable( object, {__index = Wolf})
	return object
end

function Wolf:update(dt, key)
	-- update animation
	if self.stunTime <= 0 then self.wolfAnimation:update(dt) end

	-- update timers
	if self.patternTimer > 0 then self.patternTimer = self.patternTimer - dt end
	if self.status == "dead" and self.despawnTimer > 0 then self.despawnTimer = self.despawnTimer - dt end
	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	if self.stunTime > 0 then self.stunTime = self.stunTime - dt end

	if self.status == "alive" then
		-- if you are facing player
		if (self.direction == "right" and self.x - player.x < 0) or (self.direction == "left" and self.x - player.x > 0) then
			-- if player is in range and wolf isn't standing
			if self.x - player.x <= 128 and self.x - player.x >= -128 and self.state == "walk" then
				-- if there is no obstacles in the way
				local direction
				if self.direction == "left"  then direction = -1 end
				if self.direction == "right" then direction =  1 end
				if  not isColliding(currentMap, self.x + 35 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 1*16 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 2*16 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 3*16 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 4*16 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 5*16 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 6*16 * direction, self.y + 23) and
					not isColliding(currentMap, self.x + 35 + 7*16 * direction, self.y + 23) and
					--player is in similar y position
					player.y > self.y and player.y < self.y + 32 and
					-- landing area
					isColliding(currentMap, self.x + (35 * direction) + (6*16 * direction), self.y + self.height) then
					
					self.attackDestination = 158
					self.state = "attack"
				end
			end
		end

		-- walk
		if self.state == "walk" then
			local direction = self.x - player.x
			if direction > 0 then
				self.direction = "left"
				self.wolfAnimation = self.animWalkLeft
			else
				self.direction = "right"
				self.wolfAnimation = self.animWalkRight
			end

			local offset
			if self.direction == "left" then
				offset = -35
			elseif self.direction == "right" then
				offset = 35
			end

			-- if is colliding with wall/hole in front - stop | if not, walk
			if  isColliding(currentMap, self.x + offset, self.y + self.height - 1)
				or not (isColliding(currentMap, self.x + offset, self.y + self.height)) then
				self.patternTimer = 1
				self.state = "stand"	
			else
				local direction
				if self.direction == "right" then
					direction = 1
				else
					direction = -1
				end
				self.x = self.x + (self.walkspeed * dt) * direction
			end
		end -- /if state == "walk"

		if self.state == "stand" then
			if self.direction == "right" then
				self.wolfAnimation = self.animStandRight
			else
				self.wolfAnimation = self.animStandLeft
			end

			if self.patternTimer <= 0 then
				self.state = "walk"
			end
		end -- /if state == "stand"

		if self.state == "cooldown" then
			-- powinno działać bez ustawienia animacji, bo ustawiona jest w ataku
			if self.patternTimer <= 0 then
				self.state = "walk"
			end
		end

		if self.state == "attack" then
			if self.attackDestination > 0 then
				-- Przeleć kawałek z animacją
				local direction
				if self.direction == "right" then
					direction =  1
					self.wolfAnimation = self.animAttackRight
				end
				if self.direction == "left"  then
					direction = -1
					self.wolfAnimation = self.animAttackLeft
				end

				local distance = 450 * dt
				self.x = self.x + distance * direction
				self.attackDestination = self.attackDestination - distance
			else
				-- zmienic na animacje cooldownu
				if self.direction == "right" then
					self.wolfAnimation = self.animCooldownRight
				else
					self.wolfAnimation = self.animCooldownLeft
				end
				-- chuj z randomem
				--local random = math.random(12, 16)
				--self.patternTimer = random / 10
				self.patternTimer = 1.4
				self.state = "cooldown"
				self.animAttackLeft:gotoFrame(1)
				self.animAttackLeft:resume()
				self.animAttackRight:gotoFrame(1)
				self.animAttackRight:resume()
				self.animCooldownLeft:gotoFrame(1)
				self.animCooldownLeft:resume()
				self.animCooldownRight:gotoFrame(1)
				self.animCooldownRight:resume()
			end
		end
		self:hitDetection()
	end -- /if status == "alive"

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 640
		or self.x > camX + windowWidth + 480
		or self.y < camY - 480
		or self.y > camY + windowHeight + 480 then

		spawns[self.spawnNumber].status = "pending"
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- drop heart
		local rand = math.random(1,100)
		if rand <= 100 then
			table.insert( entities, smallHeart:new(self.x, self.y + 16))
		end
		self.animDieRight:gotoFrame(1)
		self.animDieRight.status = "playing"
		self.animDieLeft:gotoFrame(1)
		self.animDieLeft.status = "playing"
		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		-- remove entity
		entities[key] = nil
	end
end

function Wolf:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 30
	box[2] = self.y + 7
	box[3] = self.x + 28
	box[4] = self.y + 39

	self.hitbox = {}
	self.hitbox[1] = box

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.5
			self.health = self.health - player.whipDamage
			self.stunTime = self.stunDuration
			-- check if skeleton is 'dead'
			if self.health <= 0 then
				-- status = dead
				-- if despawnTimer <= 0 then removeEntity = true
				-- play sound when timer is full
				-- start animation
				self.status = "dead"
				if self.direction == "right" then
					self.wolfAnimation = self.animDieRight
				else
					self.wolfAnimation = self.animDieLeft
				end
				if self.despawnTimer == 3 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 12, 20))
				end
			end
		end
	end
end

function Wolf:draw()
	if self.status == "alive" then
		if self.invulnerableTimer > 0 then
			love.graphics.setColor( math.random(10,255), math.random(10,255), math.random(10,255) )
			if self.state == "attack" then
				self.wolfAnimation:draw(wolfImage2, math.floor(self.x) - 39, math.floor(self.y))
			else
				self.wolfAnimation:draw(wolfImage, math.floor(self.x) - 34, math.floor(self.y))
			end
			love.graphics.setColor(255, 255, 255)
		else
			if self.state == "attack" then
				self.wolfAnimation:draw(wolfImage2, math.floor(self.x) - 39, math.floor(self.y))
			else
				self.wolfAnimation:draw(wolfImage, math.floor(self.x) - 34, math.floor(self.y))
			end
		end
	else
		self.wolfAnimation:draw(wolfImage3, math.floor(self.x) - 34, math.floor(self.y))
	end
end

function Wolf:keypressed(key, unicode)
end