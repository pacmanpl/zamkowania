Maggot = {}

local maggotImage = love.graphics.newImage("enemies/maggot/sprite.png")

local g = anim8.newGrid(69, 72, maggotImage:getWidth(), maggotImage:getHeight())

function Maggot:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		width = 69,
		height = 72,
		walkSpeed = 35,
		direction = "none",
		spawnNumber = spawnID,
		animMaggotDieRight	= anim8.newAnimation(g('1-18',2), 0.07, 'pauseAtEnd'):flipH(),
		animMaggotDieLeft	= anim8.newAnimation(g('1-18',2), 0.07, 'pauseAtEnd'),
		animMaggotWalkRight	= anim8.newAnimation(g('1-4',1), 0.1):flipH(),
		animMaggotWalkLeft	= anim8.newAnimation(g('1-4',1), 0.1),
		animMaggotAttackRight	= anim8.newAnimation(g('1-11',3), 0.07, 'pauseAtEnd'):flipH(),
		animMaggotAttackLeft	= anim8.newAnimation(g('1-11',3), 0.07, 'pauseAtEnd'),
		pattern = "idle", 	-- idle, step
		timer = 0,		 	-- timer for patterns
		attackTimer = 0.28,	-- timer for attack
		attacked = false,
		attackTimer2 = 0.35,-- timer for attack2
		attacked2 = false,
		despawnTimer = 1.26, -- timer for death animation, after139

		damage = 1,		-- damage dealt on hit
		hitbox = nil,
		status = "alive",
		stunTime = 0,			-- how much stun is left
		stunDuration = 0.15,	-- how much time entity will be stuned
		invulnerableTimer = 0,	-- how much time entity will be invulnerable
		health = 3
	}
	object.maggotAnimation = object.animMaggotWalkLeft
	setmetatable( object, {__index = Maggot})
	return object
end

function Maggot:update(dt, key)
	-- update animation
	if self.stunTime <= 0 then self.maggotAnimation:update(dt) end
	-- take off dt from stun time
	if self.stunTime > 0 then self.stunTime = self.stunTime - dt end
	-- update timer for death animation
	if self.status == "dead" and self.despawnTimer > 0 then self.despawnTimer = self.despawnTimer - dt end
	-- update invulnerable time
	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	-- update pattern timer
	if self.timer > 0 then self.timer = self.timer - dt end

	if self.status == "alive" then

		if self.pattern == "idle" then

			local playerPosition = player.x - self.x
			if playerPosition <= 0 then
				self.direction = "left"
			else
				self.direction = "right"
			end

			local rand = math.random(1, 100)
			if rand > 0 and rand <= 20 then
				self.pattern = "walkRight"
				self.timer = math.random(2, 5)
				self.timer = self.timer / 10

				if self.direction == "left" then
					self.maggotAnimation = self.animMaggotWalkLeft
				else
					self.maggotAnimation = self.animMaggotWalkRight
				end

			elseif rand > 20 and rand <= 40 then
				self.pattern = "walkLeft"
				self.timer = math.random(2, 5)
				self.timer = self.timer / 10

				if self.direction == "left" then
					self.maggotAnimation = self.animMaggotWalkLeft
				else
					self.maggotAnimation = self.animMaggotWalkRight
				end
				
			elseif rand > 40 and rand <= 100 then
				self.pattern = "attack"
				self.timer = 0.77
				self.attackTimer = 0.28
				self.attacked = false
				self.attackTimer2 = math.random(34, 35)
				self.attackTimer2 = self.attackTimer2 / 100
				self.attacked2 = false

				if self.direction == "left" then
					self.maggotAnimation = self.animMaggotAttackLeft
				else
					self.maggotAnimation = self.animMaggotAttackRight
				end
				
			end

		elseif self.pattern == "walkRight" then

			if self.timer <= 0 then
				self.pattern = "idle"
			else
				if (isColliding(currentMap, self.x + 34, self.y) and
					isColliding(currentMap, self.x + 34, self.y + self.height))
					or isColliding(currentMap, self.x + 34, self.y + self.height -1) then
					-- go left instead
					if self.stunTime <= 0 then
						self.x = self.x - (self.walkSpeed * dt)
					end
				else
					-- go right
					if self.stunTime <= 0 then
						self.x = self.x + (self.walkSpeed * dt)
					end
				end
			end

		elseif self.pattern == "walkLeft" then

			if self.timer <= 0 then
				self.pattern = "idle"
			else
				if (isColliding(currentMap, self.x - 34, self.y) and
					isColliding(currentMap, self.x - 34, self.y + self.height))
					or isColliding(currentMap, self.x - 34, self.y + self.height -1) then
					-- go right instead
					if self.stunTime <= 0 then
						self.x = self.x + (self.walkSpeed * dt)
					end
				else
					-- go left
					if self.stunTime <= 0 then
						self.x = self.x - (self.walkSpeed * dt)
					end
				end
			end

		elseif self.pattern == "attack" then
			if self.timer <= 0 then
				self.pattern = "idle"
				self.animMaggotAttackRight:gotoFrame(1)
				self.animMaggotAttackRight.status = "playing"
				self.animMaggotAttackLeft:gotoFrame(1)
				self.animMaggotAttackLeft.status = "playing"
			else
				if self.attackTimer  > 0 then self.attackTimer  = self.attackTimer  - dt end
				if self.attackTimer2 > 0 then self.attackTimer2 = self.attackTimer2 - dt end

				-- Attack if player is in 400 pixels range
				if self.x - player.x > -200 and self.x - player.x < 200 then
					--		ATTACK #1
					if self.attackTimer <= 0 and self.attacked == false then
						self.attacked = true
						if self.direction == "left" then
																-- x,           y,        xVelocity, yVelocity, gravity, damage
							--table.insert(entities, meatBall:new(self.x, self.y + 44, -math.random(100, 130), -math.random(300, 450), 800, 1))
							table.insert(entities, meatBall:new(self.x, self.y + 44, player.x - self.x, -400, 800, 0))
						else
							--table.insert(entities, meatBall:new(self.x + 35, self.y + 44, math.random(100, 130), -math.random(300, 450), 800, 1))
							table.insert(entities, meatBall:new(self.x + 35, self.y + 44, player.x - self.x - 35, -400, 800, 0))
						end
					end

					-- 		ATTACK #2
					if self.attackTimer2 <= 0 and self.attacked2 == false then
						self.attacked2 = true
						if self.direction == "left" then
																-- x,           y,        xVelocity, yVelocity, gravity, damage
							table.insert(entities, meatBall:new(self.x, self.y + 44, player.x - self.x + (player.xVelocity / 2), -200, 400, 0))
						else
							table.insert(entities, meatBall:new(self.x + 35, self.y + 44, player.x - self.x - 35 + (player.xVelocity / 1.5), -400, 800, 0))
						end
					end
				end
			end
		end

	end -- /if status == alive

	self:hitDetection()

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 320
		or self.x > camX + windowWidth + 320
		or self.y < camY - 320
		or self.y > camY + windowHeight + 320 then

		spawns[self.spawnNumber].status = "pending"
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- drop heart
		local rand = math.random(1,100)
		if rand <= 100 then
			table.insert( entities, smallHeart:new(self.x, self.y + 16))
		end
		self.animMaggotDieRight:gotoFrame(1)
		self.animMaggotDieRight.status = "playing"
		self.animMaggotDieLeft:gotoFrame(1)
		self.animMaggotDieLeft.status = "playing"
		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		-- remove entity
		entities[key] = nil
	end
end

function Maggot:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 14
	box[2] = self.y + 15
	box[3] = self.x + 14
	box[4] = self.y + 64

	self.hitbox = {}
	self.hitbox[1] = box

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.5
			self.health = self.health - player.whipDamage
			self.stunTime = self.stunDuration
			-- check if maggot is 'dead'
			if self.health <= 0 then
				-- status = dead
				-- if despawnTimer <= 0 then removeEntity = true
				-- play sound when timer is full
				-- start animation
				self.status = "dead"
				if self.direction == "right" then
					self.maggotAnimation = self.animMaggotDieRight
				else
					self.maggotAnimation = self.animMaggotDieLeft
				end
				if self.despawnTimer == 1.86 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 12, 20))
				end
			end
		end
	end
end

function Maggot:draw()
	if self.invulnerableTimer > 0 then
		love.graphics.setColor( math.random(10,255), math.random(10,255), math.random(10,255) )
		self.maggotAnimation:draw(maggotImage, math.floor(self.x) - (32 / 2), math.floor(self.y))
		love.graphics.setColor(255, 255, 255)
	else
		self.maggotAnimation:draw(maggotImage, math.floor(self.x) - (32 / 2), math.floor(self.y))
	end
end

function Maggot:keypressed()
end