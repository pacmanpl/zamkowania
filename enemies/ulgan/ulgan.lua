Ulgan = {}

local ulganImage  = love.graphics.newImage("enemies/ulgan/lower-sprite.png")
local ulganImage2 = love.graphics.newImage("enemies/ulgan/upper-sprite.png")

local g = anim8.newGrid(86, 77, ulganImage:getWidth(), ulganImage:getHeight())
local k = anim8.newGrid(107, 75, ulganImage2:getWidth(), ulganImage2:getHeight())

function Ulgan:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		spawnNumber = spawnID,
		animFly1 = anim8.newAnimation(g('1-6',1), 0.5),
		animFly2 = anim8.newAnimation(k('1-6',1), 0.7),
		despawnTimer = 2,
		spawnWalls = true,

		platforms = {},
		platformsCount = 0,
		maxPlatforms = 10,
		platformTimer = 2,

		health = 100,
		damage = 1,
		hitbox = nil,
		status = "alive",
		invulnerableTimer = 0
	}
	object.animation1 = object.animFly1
	object.animation2 = object.animFly2
	setmetatable( object, {__index = Ulgan} )
	return object
end

function Ulgan:update(dt, key)
	self.animation1:update(dt)
	self.animation2:update(dt)

	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	if self.status == "dead" and self.despawnTimer > 0 then self.despawnTimer = self.despawnTimer - dt end
	if self.platformTimer > 0 then self.platformTimer = self.platformTimer - dt end

	if self.spawnWalls then createBossWall(47, 35, 17, 35); self.spawnWalls = false end

	if self.status == "alive" then
		-- platform generator
		if self.platformTimer <= 0 then
			local position, velocity
			position = math.random(29*16, 39*16)
			velocity = 50
			-- spawn platform				       x	    y     size  style  xVel     yVel    xDist  yDist      delay
			local platform = Platform:new(position, 47 * 16,  2,   {5,5},   0,   -velocity,   0,    -26 * 16, 0)
			table.insert( mapElements, platform )



			position = math.random(29, 39)
			velocity = 50
			-- spawn platform				       x	    y     size  style  xVel     yVel    xDist  yDist      delay
			local platform = Platform:new(position * 16, 47 * 16,  2,   {5,5},   0,   -velocity,   0,    -26 * 16, 0)
			table.insert( mapElements, platform )



			-- reset timer
			self.platformTimer = math.random(8, 28)
			self.platformTimer = self.platformTimer / 10
		end

		self:hitDetection()
	end

	-- despawning platforms
	for k, element in pairs( mapElements ) do
		if element.y < 32 * 16 then mapElements[k] = nil end
	end

	-- despawning
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		--self.animDie:gotoFrame(1)
		--self.animDie.status = "playing"

		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		-- remove entity
		entities[key] = nil
	end

	-- block player from escaping
	if self.status == "alive" then
		if player.xVelocity > 0 and player.x > 45 * 16 then player.x = 45 * 16 end
		if player.xVelocity < 0 and player.x < 19 * 16 then player.x = 19 * 16 end
	end
end

function Ulgan:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x + 20
	box[2] = self.y
	box[3] = self.x + 34
	box[4] = self.y + 23

	self.hitbox = {}
	self.hitbox[1] = box

	local box = {}
	box[1] = self.x
	box[2] = self.y + 24
	box[3] = self.x + 34
	box[4] = self.y + 68

	self.hitbox[2] = box

	local box = {}
	box[1] = self.x
	box[2] = self.y + 68
	box[3] = self.x + 25
	box[4] = self.y + 122

	self.hitbox[3] = box

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.5
			self.health = self.health - player.whipDamage
			self.stunTime = self.stunDuration
			-- check if skeleton is 'dead'
			if self.health <= 0 then
				-- status = dead
				-- if despawnTimer <= 0 then removeEntity = true
				-- play sound when timer is full
				-- start animation
				self.status = "dead"
				if self.direction == "right" then
					self.wolfAnimation = self.animDieRight
				else
					self.wolfAnimation = self.animDieLeft
				end
				if self.despawnTimer == 3 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 12, 20))
				end
			end
		end
	end
end

function Ulgan:draw()
	self.animation1:draw(ulganImage,  math.round(self.x - 43), math.round(self.y + 75))
	self.animation2:draw(ulganImage2, math.round(self.x - 48), math.round(self.y))
	love.graphics.rectangle("fill", self.x, self.y, 1,1)
end

function Ulgan:keypressed(key, unicode)
end