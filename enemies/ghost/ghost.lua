Ghost = {}

local ghostImage = love.graphics.newImage("enemies/ghost/sprite.png")

local g = anim8.newGrid(45, 75, ghostImage:getWidth(), ghostImage:getHeight())

function Ghost:new(spawnID, x, y)
	local object = {
		x = x,
		y = y,
		width = 45,
		height = 75,
		walkSpeed = 35,
		direction = "none",
		spawnNumber = spawnID,
		animGhostDieRight	= anim8.newAnimation(g('1-12',2), 0.07, 'pauseAtEnd'):flipH(),
		animGhostDieLeft	= anim8.newAnimation(g('1-12',2), 0.07, 'pauseAtEnd'),
		animGhostSpawnRight	= anim8.newAnimation(g('12-1',2), 0.07, 'pauseAtEnd'):flipH(),
		animGhostSpawnLeft	= anim8.newAnimation(g('12-1',2), 0.07, 'pauseAtEnd'),
		animGhostWalkRight	= anim8.newAnimation(g('1-4',1), 0.1):flipH(),
		animGhostWalkLeft	= anim8.newAnimation(g('1-4',1), 0.1),
		spawnTimer   = 0.84, -- timer for spawn animation
		despawnTimer = 0.84,	 -- timer for death animation
		dontMove = false,

		damage = 1,		-- damage dealt on hit
		hitbox = nil,
		status = "spawn",
		invulnerableTimer = 0,	-- how much time entity will be invulnerable
		health = 3
	}
	object.ghostAnimation = object.animGhostWalkLeft
	setmetatable( object, {__index = Ghost})
	return object
end

function Ghost:update(dt, key)
	-- update animation
	self.ghostAnimation:update(dt)
	-- update timer for death animation
	if self.status == "dead" and self.despawnTimer > 0 then self.despawnTimer = self.despawnTimer - dt end
	-- update timer for spawn animation
	if self.status == "spawn" and self.spawnTimer > 0 then self.spawnTimer = self.spawnTimer - dt end
	-- update invulnerable time
	if self.invulnerableTimer > 0 then self.invulnerableTimer = self.invulnerableTimer - dt end
	-- set starting direction and animation
	if self.direction == "none" then
		local playerPosition = player.x - self.x
		if playerPosition <= 0 then
			self.direction = "left"
		else
			self.direction = "right"
		end
	end
	if self.status == "spawn" then
		if self.spawnTimer <= 0 then
			self.status = "alive"
		end
		if self.direction == "right" then
				self.ghostAnimation = self.animGhostSpawnRight
		else
				self.ghostAnimation = self.animGhostSpawnLeft
		end
	end
	if self.status == "alive" then
		if self.direction == "right" then
				self.ghostAnimation = self.animGhostWalkRight
		else
				self.ghostAnimation = self.animGhostWalkLeft
		end
	end -- /if status == alive

	-- check for collision in front of ghost
	local offset
	if self.direction == "left" then offset = -20 else
		offset = 20
	end

	if self.status ~= "dead" then
		if (isColliding(currentMap, self.x + 20, self.y + self.height - 1) 	or 
			not isColliding(currentMap, self.x + 20, self.y + self.height))	and
		   (isColliding(currentMap, self.x - 20, self.y + self.height - 1)	or 
			not isColliding(currentMap, self.x - 20, self.y + self.height)) then
			
			self.dontMove = true
		else
			self.dontMove = false
			if isColliding(currentMap, self.x + offset, self.y + self.height - 1) then
				-- turn around
				if self.direction == "right" then
					self.direction = "left"
				elseif self.direction == "left" then
					self.direction = "right"
				end
			else -- when no obstacles check if there is a floor
				if isColliding(currentMap, self.x + offset, self.y + self.height) then
					--  great c:
				else
					-- turn around
					if self.direction == "right" then
						self.direction = "left"
					elseif self.direction == "left" then
						self.direction = "right"
					end
				end
			end
		end
	end

	-- move it
	if self.dontMove == false then
		if self.direction == "right" then
			self.x = self.x + (self.walkSpeed * dt)
		else
			self.x = self.x + (self.walkSpeed * dt) * -1
		end
	end

	self:hitDetection()

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 64
		or self.x > camX + windowWidth + 64
		or self.y < camY - 64
		or self.y > camY + windowHeight + 64 then

		spawns[self.spawnNumber].status = "pending"
		-- decrase count
		if spawns[self.spawnNumber].count ~= nil then
			spawns[self.spawnNumber].count = spawns[self.spawnNumber].count - 1
		end
		entities[key] = nil
	end
		-- if entity is dead
	if self.status == "dead" and self.despawnTimer <= 0 then
		-- drop heart
		--local rand = math.random(1,100)
		--if rand <= 100 then
		--	table.insert( entities, smallHeart:new(self.x, self.y + 16))
		--end
		self.animGhostDieRight:gotoFrame(1)
		self.animGhostDieRight.status = "playing"
		self.animGhostDieLeft:gotoFrame(1)
		self.animGhostDieLeft.status = "playing"
		-- change spawn state to pending
		spawns[self.spawnNumber].status = "pending"
		-- decrase count
		if spawns[self.spawnNumber].count ~= nil then
			spawns[self.spawnNumber].count = spawns[self.spawnNumber].count - 1
		end
		-- remove entity
		entities[key] = nil
	end
end

function Ghost:hitDetection()
	-- update hitbox
	local box = {}
	box[1] = self.x - 13
	box[2] = self.y + 12
	box[3] = self.x + 13
	box[4] = self.y + 74

	self.hitbox = {}
	self.hitbox[1] = box

	-- check if whip is whiping
	if player.isAttacking and
		player.whipHitbox[1] ~= nil and
		self.invulnerableTimer <= 0 then
		if (player.whipHitbox[1] <= self.hitbox[1][3] and player.whipHitbox[3] >= self.hitbox[1][1] and
			player.whipHitbox[2] <= self.hitbox[1][4] and player.whipHitbox[4] >= self.hitbox[1][2]) and
			self.status == "alive" then
			playSound("WhipHit")
			self.invulnerableTimer = 0.4
			self.health = self.health - player.whipDamage
			-- check if ghost is 'dead'
			if self.health <= 0 then
				self.status = "dead"
				if self.direction == "right" then
					self.ghostAnimation = self.animGhostDieRight
				else
					self.ghostAnimation = self.animGhostDieLeft
				end
				if self.despawnTimer == 0.84 then
					playSound("EnemyDeath")
					-- drop soul
					table.insert( particles, Particle:new("soul", self.x + 4, self.y + 32, 12, 10))
				end
			end
		end
	end
end

function Ghost:draw()
	if self.invulnerableTimer > 0 then
		love.graphics.setColor( math.random(10,255), math.random(10,255), math.random(10,255) )
		self.ghostAnimation:draw(ghostImage, math.floor(self.x) - (45 / 2), math.floor(self.y))
		love.graphics.setColor(255, 255, 255)
	else
		self.ghostAnimation:draw(ghostImage, math.floor(self.x) - (45 / 2), math.floor(self.y))
	end
end

function Ghost:keypressed()
end