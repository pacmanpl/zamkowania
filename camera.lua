camera = {}
camera._x = 0
camera._y = 0
camera.scaleX = 1
camera.scaleY = 1
camera.rotation = 0
 
function camera:set()
	love.graphics.push()
	love.graphics.rotate(-self.rotation)
	love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
	love.graphics.translate(-self._x, -self._y)
end
 
function camera:unset()
	love.graphics.pop()
end
 
function camera:move(dx, dy)
	self._x = self._x + (dx or 0)
	self._y = self._y + (dy or 0)
end
 
function camera:rotate(dr)
	self.rotation = self.rotation + dr
end
 
function camera:scale(sx, sy)
	sx = sx or 1
	self.scaleX = self.scaleX * sx
	self.scaleY = self.scaleY * (sy or sx)
end
 
function camera:setX(value)
	if self._bounds then
		self._x = math.clamp(value, self._bounds.x1, self._bounds.x2)
	else
		self._x = value
	end
end
 
function camera:setY(value)
	if self._bounds then
		self._y = math.clamp(value, self._bounds.y1, self._bounds.y2)
	else
		self._y = value
	end
end
 
function camera:setPosition(x, y)
	if x then self:setX(x) end
	if y then self:setY(y) end
end
 
function camera:setScale(sx, sy)
	self.scaleX = sx or self.scaleX
	self.scaleY = sy or self.scaleY
end
 
function camera:getBounds()
	return unpack(self._bounds)
end
 
function camera:setBounds(x1, y1, x2, y2)
	self._bounds = { x1 = x1, y1 = y1, x2 = x2, y2 = y2 }
end

--[[          --        THIS IS MAH SPACE        --          ]]--
function updateCamera(dt)
	local prevCamY
	if player.onFloor then
			camX = math.round(player.x, 0.5) - 200
			--camY = camY + ((player.y - 180 - camY) * dt * 5) FU
			if camY - (player.y - 180) < 0 then
				camY = camY + (125*dt)
			end
			if camY - (player.y - 180) > 0 then
				camY = camY - (125*dt)
			end
			if camY - (player.y - 180) <= 1 and camY - (player.y - 180) >= -1 then
				camY = player.y - 180
			end
	end
	if not player.onFloor and not player.onStairs then
			camX = math.round(player.x) - 200
			-- falling border
			if math.round(camY) + 400 - math.round(player.y) <= 183 then
				camY = player.y - 216
			end
			if math.round(player.y) - math.round(camY) <= 83 then
				camY = player.y - 83
			end
	end
	if player.onStairs then
			camX = math.round(player.x) - 200
			camY = camY + ((math.round(player.y) - 150 - camY) * dt * 5)

			if math.round(camY) + 400 - math.round(player.y) <= 231 and
				 math.round(camY) + 400 - math.round(player.y) >= 230 and love.keyboard.isDown('s') then
				camY = player.y - 169
			end

			if math.round(camY) + 400 - math.round(player.y) >= 212 and
				 math.round(camY) + 400 - math.round(player.y) <= 213 and love.keyboard.isDown('s') then
				camY = player.y - 188
			end

			if math.round(player.y) - math.round(camY) <= 143 and love.keyboard.isDown('w')  then
				camY = player.y - 143
			end

	end
	if player.x < windowWidth / 2 then camX = 0 end
	if player.x - (currentMap.width * 16) > -windowWidth / 2 then camX = (currentMap.width * 16) - windowWidth end
	--if player.y < windowHeight / 2 then camY = 0 end
	if camY < 0 then camY = 0 end
	if camY > (currentMap.height * 16) - windowHeight then camY = (currentMap.height * 16) - windowHeight end

	camera:setPosition(math.round(camX), math.round(camY))
end