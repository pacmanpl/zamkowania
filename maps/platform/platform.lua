Platform = {}

local platformImage = love.graphics.newImage("maps/platform/sprite.png")

function Platform:new(x, y, size, style, xVelocity, yVelocity, xDistance, yDistance, delay)
	local object = {
		x = x,
		y = y,
		size = size,
		style = style,
		xVelocity = xVelocity,
		yVelocity = yVelocity,
		xDistance = xDistance,
		yDistance = yDistance,
		direction = 1,
		traveled = 0,
		delay = delay
	}

	local tiles = {}
	for i=0, object.size - 1 do
		tiles[i+1] = love.graphics.newQuad((object.style[i+1]-1) * 16, 0, 16, 16, platformImage:getWidth(), platformImage:getHeight())
	end
	object.style = tiles
	setmetatable( object, {__index = Platform})
	return object
end

function Platform:update(dt)
	if self.delay >= 0 then self.delay = self.delay - dt end

	if self.delay <= 0 then
		-- Collisions
		local playerCollide
		if player.yVelocity < 200 then
			if (player.yVelocity > 0 or player.onPlatform) and not (player.onStairs)	and
				((player.x - 9 > self.x and player.x - 9 < self.x + (self.size * 16) 	and
				  math.ceil(player.y) + 31 == math.ceil(self.y)) 						or
				  (player.x + 8 > self.x and player.x + 8 < self.x + (self.size * 16) 	and
				  math.ceil(player.y) + 31 == math.ceil(self.y))						or
				  (player.x > self.x and player.x < self.x + (self.size * 16)			and
				  math.ceil(player.y) + 31 >= math.ceil(self.y) and math.ceil(player.y) + 31 < math.ceil(self.y) + 16))	then
				playerCollide = true
			else
				playerCollide = false
			end
		end
		if player.yVelocity >= 200 then
			if (player.yVelocity > 0 or player.onPlatform) and not (player.onStairs)	and
				((player.x - 9 > self.x and player.x - 9 < self.x + (self.size * 16) 	and
				  math.ceil(player.y) + 31 >= math.ceil(self.y) and math.ceil(player.y) + 31 < math.ceil(self.y) + 16)	or
				  (player.x + 8 > self.x and player.x + 8 < self.x + (self.size * 16) 	and
				  math.ceil(player.y) + 31 >= math.ceil(self.y) and math.ceil(player.y) + 31 < math.ceil(self.y) + 16)	or
				  (player.x > self.x and player.x < self.x + (self.size * 16)			and
				  math.ceil(player.y) + 31 >= math.ceil(self.y) and math.ceil(player.y) + 31 < math.ceil(self.y) + 16))	then
				playerCollide = true
			else
				playerCollide = false
			end
		end

		if playerCollide then

			player.onPlatform = true
			player.onPlatform2 = true
			player:collide("hitFloor")

		-- write down the spot of a player
			local offset = math.floor(player.x) - math.floor(self.x)
		-- move the platform
			self.x = self.x + (self.xVelocity * dt) * self.direction
			self.y = self.y + (self.yVelocity * dt) * self.direction
			self.traveled = self.traveled + (self.xVelocity * dt) * self.direction + (self.yVelocity * dt) * self.direction

		-- change player position
			if player.state == "stand" or player.state == "crouchRight" or player.state == "crouchLeft" or player.isAttacking then
				--local offset = math.round(player.x - self.x, 0.5)
				player.x = self.x + offset
			else
				player.x = player.x + (self.xVelocity * dt) * self.direction
			end
		-- increase platform velocity to fix parallax scrolling
			player.platformVelocity = self.xVelocity * self.direction
		--player.y = player.y + (self.yVelocity * dt) * self.direction	
			player.y = self.y - 31
				--local offset = math.round(player.y + 31 - self.y, 0.5)
				--player.y = self.y + offset - 31
		else

		-- move the platform
			self.x = self.x + (self.xVelocity * dt) * self.direction
			self.y = self.y + (self.yVelocity * dt) * self.direction
			self.traveled = self.traveled + (self.xVelocity * dt) * self.direction + (self.yVelocity * dt) * self.direction
		-- remove platform velocity if player isn't on platform
			if player.onPlatform == false then player.platformVelocity = 0 end
		end
		
	--							██╗  ██╗
	--							╚██╗██╔╝
	--							 ╚███╔╝ 
	--							 ██╔██╗ 
	--							██╔╝ ██╗
	--							╚═╝  ╚═╝
	        
			-- if platform moves right and it's not "returning"
			if self.xVelocity > 0 and self.direction == 1 then
				if self.traveled >= self.xDistance then
					self.direction = self.direction * -1
				end
			end
			-- if platform moves left and is "returning"
			if self.xVelocity > 0 and self.direction == -1 then
				if self.traveled <= 0 then
					self.direction = self.direction * -1
				end
			end

			-- if platform moves left and it's not "returning"
			if self.xVelocity < 0 and self.direction == 1 then
				if self.traveled <= self.xDistance then
					self.direction = self.direction * -1
				end
			end
			-- if platform moves right and is "returning"
			if self.xVelocity < 0 and self.direction == -1 then
				if self.traveled >= 0 then
					self.direction = self.direction * -1
				end
			end

	--							██╗   ██╗
	--							╚██╗ ██╔╝
	--							 ╚████╔╝ 
	--							  ╚██╔╝  
	--							   ██║   
	--							   ╚═╝   

			-- if platform moves down and it's not "returning"
			if self.yVelocity > 0 and self.direction == 1 then
				if self.traveled >= self.yDistance then
					self.direction = self.direction * -1
				end
			end
			-- if platform moves up and is "returning"
			if self.yVelocity > 0 and self.direction == -1 then
				if self.traveled <= 0 then
					self.direction = self.direction * -1
				end
			end

			-- if platform moves up and it's not "returning"
			if self.yVelocity < 0 and self.direction == 1 then
				if self.traveled <= self.yDistance then
					self.direction = self.direction * -1
				end
			end
			-- if platform moves down and is "returning"
			if self.yVelocity < 0 and self.direction == -1 then
				if self.traveled >= 0 then
					self.direction = self.direction * -1
				end
			end
	end
end

function Platform:draw()
	if self.x + self.size * 16 > camX - 32 and self.x < camX + windowWidth + 32 and
		self.y > camY - 32 and self.y < camY + windowHeight + 32 then
		--love.graphics.rectangle("fill", math.floor(self.x), math.floor(self.y), self.size * 16, 16)
		for i=0, #self.style - 1 do
			love.graphics.draw(platformImage, self.style[i+1], math.round(self.x + i * 16), math.round(self.y))
		end
	end
	--love.graphics.print("  y: "..math.floor(player.x) - math.floor(self.x), player.x, player.y)
	--love.graphics.print("  y: "..self.y, player.x, player.y + 15)
	--love.graphics.rectangle("fill", self.x, self.y, 20, 16)
	--love.graphics.rectangle("fill", player.x, player.y, 1, 1)
	--love.graphics.rectangle("fill", self.x, self.y, self.size * 16, 16)
end