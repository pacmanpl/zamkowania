arrowLauncher = {}

local arrowLauncherImage = love.graphics.newImage("maps/arrowLauncher/sprite.png")

function arrowLauncher:new(x, y, style, delay, reload, direction, arrowVelocity)
	local object = {
		x = x,
		y = y,
		style = style,
		delay = delay,
		reload = reload,
		reloadTimer = 0,
		direction = direction,
		arrowVelocity = arrowVelocity,
		arrowDamage = arrowDamage
	}

	local tile = love.graphics.newQuad((object.style-1) * 34, 0, 34, 32, arrowLauncherImage:getWidth(), arrowLauncherImage:getHeight())
	object.style = tile
	setmetatable( object, {__index = arrowLauncher})
	return object
end

function arrowLauncher:update(dt)
	if self.delay > 0 then self.delay = self.delay - dt end
	if self.reloadTimer >= 0 then self.reloadTimer = self.reloadTimer - dt end

	if self.delay <= 0 then
		-- pew pew pew if in range
		if self.reloadTimer <= 0 then
			self.reloadTimer = self.reload
			if self.x > camX - windowWidth and self.x < camX + (windowWidth * 2) and
				self.y > camY - windowHeight and self.y < camY + (windowHeight * 2) then
				local offset
				if self.direction == 1 then
					offset = 33
				else
					offset = 0
				end
				table.insert(entities, launcherArrow:new(self.x + offset, self.y + 14, self.direction, self.arrowVelocity, self.arrowDamage))
			end
		end
	end
end

function arrowLauncher:draw()
	if self.x + 34 > camX - 34 and self.x < camX + windowWidth + 34 and
		self.y > camY - 34 and self.y < camY + windowHeight + 34 then

		love.graphics.draw(arrowLauncherImage, self.style, math.floor(self.x), math.floor(self.y))
	end
end