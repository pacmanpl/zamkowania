function loadSpawnPoints(currentPlace)
	spawns = {}
	--[[

	 ██████╗  ██╗        █████╗ ██████╗ ███████╗███████╗██╗   ██╗
	██╔═████╗███║       ██╔══██╗██╔══██╗██╔════╝██╔════╝██║   ██║
	██║██╔██║╚██║       ███████║██████╔╝█████╗  █████╗  ██║   ██║
	████╔╝██║ ██║       ██╔══██║██╔══██╗██╔══╝  ██╔══╝  ██║   ██║
	╚██████╔╝ ██║██╗    ██║  ██║██║  ██║███████╗██║     ╚██████╔╝
	 ╚═════╝  ╚═╝╚═╝    ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝      ╚═════╝ 
                                                             
	]]
	if currentPlace == "Arefu" then
		--make spawns
		local spawn = {
			id = 1,		-- every entity has one id from spawn
			count = 0,	-- how many entities are spawned from spawn
			cap = 0,	-- how many entities are allowed
						-- doesn't matter with regular spawns
			mode = 1,	-- mode_1 = regular spawn. mode_2 = spawn around player x position. mode_3 = spawn from sides of screen.. mode_4 = RNG
			timer = 0,	-- for random spawns <3
			minTimer = 0, -- for custom randomness :O
			maxTimer = 0, -- time is divided by 10 !11!1!!!
			x = 16 * 33,
			y = 16 * 31,
			height = 64,
			status = "active", -- active/inactive/pending
			message = {dialogMessage01},
			entity = function(id,x,y,message)
				local object = Villager:new(id, x, y, message)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
		local spawn = {
			id = 2,
			count = 0,
			cap = 0,
			mode = 1,	-- mode_1 = regular spawn. mode_2 = spawn around player x position. mode_3 = spawn from sides of screen.
			timer = 0,	-- math.random(1, 4),	-- for random spawns <3
			x = 16 * 50,
			y = 16 * 31,
			height = 64,
			status = "active", -- active/inactive/pending
			message = {dialogMessage02},
			entity = function(id,x,y,message)
				local object = Villager:new(id, x, y, message)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
		--local spawn = {
		--	id = 3,
		--	mode = 1,
		--	x = 16 * 31,
		--	y = 16 * 31,
		--	height = 64,
		--	status = "active",
		--	entity = function(id, x, y)
		--		local object = Skeleton:new(id, x, y)
		--		table.insert( entities, object )
		--	end
		--}
		--table.insert(spawns, spawn)
	end

	--[[

	 ██████╗ ██████╗         █████╗ ██████╗ ███████╗███████╗██╗   ██╗              ██╗███╗   ██╗███╗   ██╗
	██╔═████╗╚════██╗       ██╔══██╗██╔══██╗██╔════╝██╔════╝██║   ██║              ██║████╗  ██║████╗  ██║
	██║██╔██║ █████╔╝       ███████║██████╔╝█████╗  █████╗  ██║   ██║    █████╗    ██║██╔██╗ ██║██╔██╗ ██║
	████╔╝██║██╔═══╝        ██╔══██║██╔══██╗██╔══╝  ██╔══╝  ██║   ██║    ╚════╝    ██║██║╚██╗██║██║╚██╗██║
	╚██████╔╝███████╗██╗    ██║  ██║██║  ██║███████╗██║     ╚██████╔╝              ██║██║ ╚████║██║ ╚████║
	 ╚═════╝ ╚══════╝╚═╝    ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝      ╚═════╝               ╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝
                                                                                                                                                                 
	]]

	if currentPlace == "Arefu - Inn" then
		local spawn = {
			id = 1,
			mode = 1,
			x = (16 * 10) + 8,
			y = (16 * 16) + 8,
			height = 56,
			status = "active",
			message = {dialogMessage03, dialogMessage04, dialogMessage05, dialogMessage06, dialogMessage07},
			entity = function(id, x, y, message)
				local object = Innkeeper:new(id, x, y, message)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
	end

--[[

 ██████╗ ██████╗         █████╗ ██████╗ ███████╗███████╗██╗   ██╗    ███████╗██╗███████╗██╗     ██████╗ ███████╗
██╔═████╗╚════██╗       ██╔══██╗██╔══██╗██╔════╝██╔════╝██║   ██║    ██╔════╝██║██╔════╝██║     ██╔══██╗██╔════╝
██║██╔██║ █████╔╝       ███████║██████╔╝█████╗  █████╗  ██║   ██║    █████╗  ██║█████╗  ██║     ██║  ██║███████╗
████╔╝██║ ╚═══██╗       ██╔══██║██╔══██╗██╔══╝  ██╔══╝  ██║   ██║    ██╔══╝  ██║██╔══╝  ██║     ██║  ██║╚════██║
╚██████╔╝██████╔╝██╗    ██║  ██║██║  ██║███████╗██║     ╚██████╔╝    ██║     ██║███████╗███████╗██████╔╝███████║
 ╚═════╝ ╚═════╝ ╚═╝    ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝      ╚═════╝     ╚═╝     ╚═╝╚══════╝╚══════╝╚═════╝ ╚══════╝

 ]]
	if currentPlace == "Arefu Fields" then
		--local spawn = {
		--	id = 1,
		--	mode = 1,
		--	x = 16 * 50,
		--	y = 16 * 31,
		--	height = 40,
		--	status = "active",
		--	entity = function(id, x, y)
		--		local object = Wolf:new(id, x, y)
		--		table.insert( entities, object )
		--	end
		--}
		--table.insert(spawns, spawn)
		local spawn = {
			id = 1,
			mode = 1,
			x = 16 * 31,
			y = 16 * 31,
			height = 83,
			status = "active",
			entity = function(id, x, y)
				local object = SkeletonSwordsman:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
		
		local spawn = {
			id = 2,
			mode = 1,
			x = 16 * 100,
			y = 16 * 31,
			height = 75,
			status = "active",
			entity = function(id, x, y)
				local object = Scarecrow:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
	end

--[[
                                                                                                                
 ██████╗ ██╗  ██╗        █████╗ ██████╗ ███████╗███████╗██╗   ██╗     ██████╗███████╗███╗   ███╗███████╗████████╗███████╗██████╗ ██╗   ██╗
██╔═████╗██║  ██║       ██╔══██╗██╔══██╗██╔════╝██╔════╝██║   ██║    ██╔════╝██╔════╝████╗ ████║██╔════╝╚══██╔══╝██╔════╝██╔══██╗╚██╗ ██╔╝
██║██╔██║███████║       ███████║██████╔╝█████╗  █████╗  ██║   ██║    ██║     █████╗  ██╔████╔██║█████╗     ██║   █████╗  ██████╔╝ ╚████╔╝ 
████╔╝██║╚════██║       ██╔══██║██╔══██╗██╔══╝  ██╔══╝  ██║   ██║    ██║     ██╔══╝  ██║╚██╔╝██║██╔══╝     ██║   ██╔══╝  ██╔══██╗  ╚██╔╝  
╚██████╔╝     ██║██╗    ██║  ██║██║  ██║███████╗██║     ╚██████╔╝    ╚██████╗███████╗██║ ╚═╝ ██║███████╗   ██║   ███████╗██║  ██║   ██║   
 ╚═════╝      ╚═╝╚═╝    ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝      ╚═════╝      ╚═════╝╚══════╝╚═╝     ╚═╝╚══════╝   ╚═╝   ╚══════╝╚═╝  ╚═╝   ╚═╝   
                                                                                                                                          
]]

	if currentPlace == "Arefu Cemetery01" then
		local spawn = {
			id = 1,
			mode = 1,
			x = 16 * 51,
			y = 16 * 31,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Skeleton:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		local spawn = {
			id = 2,
			mode = 1,
			x = 16 * 63,
			y = 16 * 31,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Skeleton:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		local spawn = {
			id = 3,
			mode = 1,
			x = 16 * 65,
			y = 16 * 34,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Skeleton:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		local spawn = {
			id = 4,
			mode = 1,
			x = 16 * 125,
			y = 16 * 31,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Skeleton:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		local spawn = {
			id = 5,
			mode = 1,
			x = 16 * 147,
			y = 16 * 31,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Skeleton:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		local spawn = {
			id = 6,
			mode = 1,
			x = 16 * 72,
			y = 16 * 43,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Skeleton:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		--local spawn = {
		--	id = 7,
		--	mode = 2,
		--	count = 0,
		--	cap = 1,
		--	timer = 0,
		--	minTimer = 8, -- 0.8s
		--	maxTimer = 20,-- 2s
		--	area = {16 * 0, 16 * 25, 16 * 65, 16 * 36},
		--	x = 16 * 0,
		--	y = 16 * 0,
		--	height = 75,
		--	status = "active",
		--	entity = function(id, x, y)
		--		local object = Ghost:new(id, x, y)
		--		table.insert( entities, object )
		--	end
		--}
		--table.insert(spawns, spawn)

		local spawn = {
			id = 7,
			mode = 1,
			x = 16 * 36,
			y = 16 * 38,
			height = 83,
			status = "active",
			entity = function(id, x, y)
				local object = SkeletonSwordsman:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)

		local spawn = {
			id = 8,
			mode = 1,
			x = 16 * 117,
			y = 16 * 31,
			height = 83,
			status = "active",
			entity = function(id, x, y)
				local object = SkeletonSwordsman:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
	end
--[[

 ██████╗ ███████╗       ██████╗ ██╗   ██╗███╗   ██╗ ██████╗ ███████╗ ██████╗ ███╗   ██╗ ██████╗  ██╗
██╔═████╗██╔════╝       ██╔══██╗██║   ██║████╗  ██║██╔════╝ ██╔════╝██╔═══██╗████╗  ██║██╔═████╗███║
██║██╔██║███████╗       ██║  ██║██║   ██║██╔██╗ ██║██║  ███╗█████╗  ██║   ██║██╔██╗ ██║██║██╔██║╚██║
████╔╝██║╚════██║       ██║  ██║██║   ██║██║╚██╗██║██║   ██║██╔══╝  ██║   ██║██║╚██╗██║████╔╝██║ ██║
╚██████╔╝███████║██╗    ██████╔╝╚██████╔╝██║ ╚████║╚██████╔╝███████╗╚██████╔╝██║ ╚████║╚██████╔╝ ██║
 ╚═════╝ ╚══════╝╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═╝
                                                                                                    
]]

--[[

 ██████╗  ██████╗        ██████╗ ██╗   ██╗███╗   ██╗ ██████╗ ███████╗ ██████╗ ███╗   ██╗ ██████╗ ██████╗ 
██╔═████╗██╔════╝        ██╔══██╗██║   ██║████╗  ██║██╔════╝ ██╔════╝██╔═══██╗████╗  ██║██╔═████╗╚════██╗
██║██╔██║███████╗        ██║  ██║██║   ██║██╔██╗ ██║██║  ███╗█████╗  ██║   ██║██╔██╗ ██║██║██╔██║ █████╔╝
████╔╝██║██╔═══██╗       ██║  ██║██║   ██║██║╚██╗██║██║   ██║██╔══╝  ██║   ██║██║╚██╗██║████╔╝██║██╔═══╝ 
╚██████╔╝╚██████╔╝██╗    ██████╔╝╚██████╔╝██║ ╚████║╚██████╔╝███████╗╚██████╔╝██║ ╚████║╚██████╔╝███████╗
 ╚═════╝  ╚═════╝ ╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝
                                                                                                                                                                                                           
]]

	if currentPlace == "Dungeon02" then
		local spawn = {
			id = 1,
			mode = 1,
			x = 16 * 23,
			y = 16 * 36,
			height = 64,
			status = "active",
			entity = function(id, x, y)
				local object = Ulgan:new(id, x, y)
				table.insert( entities, object )
			end
		}
		table.insert(spawns, spawn)
	end
	return spawns
end

function updateSpawns(dt, map)
	for k,spawn in ipairs( spawns ) do
		if spawn.mode == 1 then
			-- spawn entity if you are in spawn range
			if camX - 64 < spawn.x
				and spawn.x < camX + windowWidth + 64
				and camY - 64 < spawn.y
				and spawn.y < camY + windowHeight + 64 then
				-- and the spawn is active, otherwise do nothing
				if spawn.status == "active" then
					spawn.entity(spawn.id, spawn.x, spawn.y - spawn.height, spawn.message)
					spawn.status = "inactive"
				end
			else -- if spawn is not in your range change status from "pending" to "active" if needed
				if spawn.status == "pending" then spawn.status = "active" end
			end
		end
		if spawn.mode == 2 then
			-- check if player is in spawn area
			if spawn.area ~= nil then
				if (player.x > spawn.area[1] and player.x < spawn.area[3]) and
					(player.y > spawn.area[2] and player.y < spawn.area[4]) then

					spawn.timer = spawn.timer - dt
					if spawn.timer <= 0 then
						spawn.timer = math.random(spawn.minTimer, spawn.maxTimer)
						spawn.timer = spawn.timer / 10
						if spawn.count < spawn.cap then
							local x = player.x
							local y = (math.floor((player.y + 32) / 16) * 16) + 0
							for i=0, 2 do
								-- try to find floor
								if isColliding(map, x, y + (16 * i)) then
									-- you know where floor is, now chose random X point on the floor tile
									local spawnX = math.random(math.floor(player.x) - (13 * 16), math.floor(player.x) + (13 * 16))
									local spawnY = (math.floor(y + (16 * i)))
									-- check if this random point is a wall
									-- if it is just don't do shit; else spawn shit
									if isColliding(map, spawnX, spawnY) and not
									   isColliding(map, spawnX, spawnY - 16) then
										spawn.entity(spawn.id, spawnX, spawnY - spawn.height)
										spawn.count = spawn.count + 1
									end
									--break
								end
							end
						end
					end
				end
			end
		end
		if spawn.mode == 3 then
			-- check if player is in spawn area
			if spawn.area ~= nil then
				if (player.x > spawn.area[1] and player.x < spawn.area[3]) and
					(player.y > spawn.area[2] and player.y < spawn.area[4]) then

					spawn.timer = spawn.timer - dt

					if spawn.timer <= 0 then
						spawn.timer = math.random(spawn.minTimer, spawn.maxTimer)
						spawn.timer = spawn.timer / 10
						if spawn.count < spawn.cap then
							local rand = math.random(1, 2)
							local spawnX
							if rand == 1 then
								-- spawn before the camera
								spawnX = camX - 32
							else
								-- spawn ahead of the camera
								spawnX = camX + 432
							end
							-- spawn on the screen with little margin
							local spawnY = math.random(camY + 100, camY + 270)

							spawn.entity(spawn.id, spawnX, spawnY - spawn.height)
							spawn.count = spawn.count + 1
						end
					end
				end
			end
		end
		if spawn.mode == 4 then
			-- check if player is in spawn area
			if spawn.area ~= nil then
				if (player.x > spawn.area[1] and player.x < spawn.area[3]) and
					(player.y > spawn.area[2] and player.y < spawn.area[4]) then

					spawn.timer = spawn.timer - dt

					if spawn.timer <= 0 then
						spawn.timer = math.random(spawn.minTimer, spawn.maxTimer)
						spawn.timer = spawn.timer / 10
						if spawn.count < spawn.cap then
							-- spawn RANDOMLY
							local spawnX = math.random(camX + 16, camX + 385)
							local spawnY = math.random(camY + 20, camY + 275)

							spawn.entity(spawn.id, spawnX, spawnY - spawn.height)
							spawn.count = spawn.count + 1
						end
					end
				end
			end
		end
	end
end