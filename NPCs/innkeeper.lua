Innkeeper = {}

local innkeeperImage = love.graphics.newImage("NPCs/innkeeperSprite.png")

local g = anim8.newGrid(29, 56, innkeeperImage:getWidth(), innkeeperImage:getHeight())

animInnkeeperStand = anim8.newAnimation(g('1-2',1), {[1]=8,[2]=0.5})

function Innkeeper:new(spawnID, x, y, message)
	local object = {
		x = x,
		y = y,
		spawnNumber = spawnID,
		dialog = message
	}
	setmetatable( object, {__index = Innkeeper})
	return object
end

function Innkeeper:draw()
	animInnkeeperStand:draw(innkeeperImage, math.floor(self.x), math.floor(self.y))
	if player.dialogState == "inactive" and
		dialogFocus == self.spawnNumber then
		dialogBoxAnimation:draw(dialogImage, math.floor(self.x) + 25, math.floor(self.y) + 10)
	end
end

function Innkeeper:update(dt)
	animInnkeeperStand:update(dt)
	dialogBoxAnimation:update(dt)

	if	player.x > self.x - 10 and
		player.x < self.x + 39 then
		if dialogState ~= "active" then
			if dialogFocus == nil then
				player.dialogState = "inactive"
				dialogFocus = self.spawnNumber
			end
		end
	else
		if dialogFocus == self.spawnNumber then
			dialogFocus = nil
			player.dialogState = nil
		end
	end

end

function Innkeeper:keypressed(key, unicode)
	if key == "o" and player.dialogState == "inactive" and dialogFocus == self.spawnNumber and player.onFloor and not player.isAttacking then
		player.dialogState = "active"
		player:stop()
		-- play little beep
		playSound("Confirm")
		-- push table into display dialog function
		local dialog = {}
		for k, message in ipairs( self.dialog ) do
			local element = {}
			element.x = self.x
			element.y = self.y
			element.image = message
			element.mode = "message"
			table.insert( dialog, element )
		end
		createDialog( dialog )
	end
end