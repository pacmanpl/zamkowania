Villager = {}

local villagerImage = love.graphics.newImage("NPCs/villagerSprite.png")

local g = anim8.newGrid(32, 64, villagerImage:getWidth(), villagerImage:getHeight())

function Villager:new(spawnID, x, y, message)
	local object = {
		x = x,
		y = y,
		width = 32,
		height = 64,
		xVelocity = 0,
		walkSpeed = 30,
		direction = "right",
		spawnNumber = spawnID,
		dialog = message,
		-- pattern variables
		patternState = "idle", -- idle, wander {wanderLeft, wanderRight}
		startingX = x, -- place where entity was spawned, it will going around this point
		timer = 0, -- timer for pattern, random seconds for idle and random for wander
		animVillagerWalkLeft	= anim8.newAnimation(g('1-8',2), 0.13):flipH(),
		animVillagerWalkRight	= anim8.newAnimation(g('1-8',2), 0.13),
		animVillagerStandLeft	= anim8.newAnimation(g(1,1), 0.1):flipH(),
		animVillagerStandRight	= anim8.newAnimation(g(1,1), 0.1)
	}
	object.villagerAnimation = object.animVillagerStandRight
	setmetatable( object, {__index = Villager})
	return object
end

function Villager:draw()
	self.villagerAnimation:draw(villagerImage, math.floor(self.x), math.floor(self.y))
	if player.dialogState == "inactive" and
		dialogFocus == self.spawnNumber then
		dialogBoxAnimation:draw(dialogImage, math.floor(self.x) + 25, math.floor(self.y) + 10)
	end
end

function Villager:update(dt, key) -- key is index of entities table
	-- update animation
	self.villagerAnimation:update(dt)
	dialogBoxAnimation:update(dt)

	-- decreasing timer with dt
	self.timer = self.timer - dt
		-- Idle pattern is on
	if self.patternState == "idle" then
		self.xVelocity = 0
		-- if timer is 0, change pattern for "wander"
		if self.timer <= 0 and player.dialogState ~= "active" then
			self.patternState = "wander"
			-- set random timer for wandering
			local min, max = 2, 4
			self.timer = math.random(min, max)
		end
	else
		if self.startingX - self.x < -80 and self.patternState == "wander" then self.patternState = "wanderLeft"
		elseif self.startingX - self.x > 80 and self.patternState == "wander" then self.patternState = "wanderRight"
		elseif self.patternState == "wander" then
			local rand = math.random(1,2)
			if rand == 1 then self.patternState = "wanderRight"
			else self.patternState = "wanderLeft" end
		end
		if self.patternState == "wanderLeft" then self.xVelocity = self.walkSpeed * -1; self.x = self.x - (self.walkSpeed * dt) end
		if self.patternState == "wanderRight" then self.xVelocity = self.walkSpeed; self.x = self.x + (self.walkSpeed * dt) end
		-- change pattern if counter is 0
		if self.timer <= 0 then
			self.patternState = "idle"
			local min, max = 3, 6
			self.timer = math.random(min, max)
		end
	end

	-- talking | if you are close to player
	if (self.x + 16 - player.x >= -20 and self.x + 8 - player.x <= 20) and
		(self.y - player.y + 32 <= 31 and self.y - player.y + 32 >= -31) then
		if player.dialogState ~= "active" then
			if dialogFocus == nil then -- if there is no focus, focus current motherfucker
				player.dialogState = "inactive"
				dialogFocus = self.spawnNumber
			end -- if someone is focused DO NOTHING
		end
	else -- if villager is not close do nothing if he's not focused
		if dialogFocus == self.spawnNumber then
			dialogFocus = nil
			player.dialogState = nil
		end
	end

	-- despawning
		-- if entity is 40 tiles away
	if self.x < camX - 640
			or self.x > camX + windowWidth + 480
			or self.y < camY - 480
			or self.y > camY + windowHeight + 480 then

		spawns[self.spawnNumber].status = "pending"
		entities[key] = nil
	end

	self:getState()
end

function Villager:getState()
	if self.xVelocity > 0 then
		self.villagerAnimation = self.animVillagerWalkRight
		self.direction = "right"
	end
	if self.xVelocity < 0 then
		self.villagerAnimation = self.animVillagerWalkLeft
		self.direction = "left"
	end
	if self.xVelocity == 0 then
		if self.direction == "right" then
			self.villagerAnimation = self.animVillagerStandRight
		else
			self.villagerAnimation = self.animVillagerStandLeft
		end
	end
end

function Villager:keypressed(key, unicode)
	if key == "o" and player.dialogState == "inactive" and dialogFocus == self.spawnNumber and player.onFloor and not player.isAttacking then
		player.dialogState = "active"
		player:stop()
		self.patternState = "idle"
		-- play little beep
		playSound("Confirm")
		-- push table into display dialog function
		local dialog = {}
		for k, message in ipairs( self.dialog ) do
			local element = {}
			element.x = self.x
			element.y = self.y
			element.image = message
			element.mode = "message"
			table.insert( dialog, element )
		end
		createDialog( dialog )
	end
end