lastPosition = 0

function updateBackground(dt, x)
	local direction = 0
	if lastPosition == 0 then lastPosition = x end
	if x > lastPosition then direction = -1 end
	if x == lastPosition then direction = 0 end
	if x < lastPosition then direction = 1 end

	local Velocity = (player.xVelocity + player.platformVelocity) * 100
	Velocity = (Velocity / 125) / 125
	if Velocity < 0 then Velocity = Velocity * -1 end

	for k in pairs(backgroundElements) do
		if backgroundElements[k].autoscroll ~= 2 then
			backgroundElements[k].x = backgroundElements[k].x + ((backgroundElements[k].speed * dt) * Velocity) * direction
		else
			backgroundElements[k].x = backgroundElements[k].x + (backgroundElements[k].autoscrollSpeed * dt)
		end

		if direction == 0 and backgroundElements[k].autoscroll == 1 then
			backgroundElements[k].x = backgroundElements[k].x + (backgroundElements[k].autoscrollSpeed * dt)
		end

		if backgroundElements[k].loop == 1 then
			if backgroundElements[k].x > windowWidth then
				backgroundElements[k].x = 0 - backgroundElements[k].size
			end
			if backgroundElements[k].x < -backgroundElements[k].size then
				backgroundElements[k].x = windowWidth
			end
		elseif backgroundElements[k].loop == 2 then
			if backgroundElements[k].x > windowWidth then
				backgroundElements[k].x = windowWidth - backgroundElements[k].size
			end
			if backgroundElements[k].x < -backgroundElements[k].size then
				backgroundElements[k].x = 0
			end
		end
	end

	lastPosition = x
end

function drawBackground()
	mapHeight = currentMap.height * currentMap.tileHeight
	for k in pairs(backgroundElements) do				-- for every element in table
		if backgroundElements[k].loop == 2 then			-- if the element is looped
			-- fill left site of screen with looped image
			for i=1, math.ceil(backgroundElements[k].x / backgroundElements[k].size) do
				love.graphics.draw(backgroundElements[k].image, math.round(camX + backgroundElements[k].x - (backgroundElements[k].size * i)), mapHeight - backgroundElements[k].y)
			end
			-- fill right site of screen with looped image
			for i=1, math.ceil((camX + windowWidth - backgroundElements[k].x + backgroundElements[k].size) / backgroundElements[k].size) do
				love.graphics.draw(backgroundElements[k].image, math.round(camX + backgroundElements[k].x + (backgroundElements[k].size * i)), mapHeight - backgroundElements[k].y)
			end
		end
		if backgroundElements[k].loop ~= 3 then
			love.graphics.draw(backgroundElements[k].image, math.round(camX + backgroundElements[k].x), mapHeight - backgroundElements[k].y)
		else
			love.graphics.draw(backgroundElements[k].image, math.round(camX), math.round(camY))
		end
	end
end

function loadBackgroundTable(place)
	backgroundElements = {}

	if place == "Arefu" then
		backgroundElements = { backgroundClouds1, 			backgroundMountain2,		backgroundMountain3,
								backgroundMountain1,		backgroundMountain5,		backgroundMountain4,		backgroundMountain6,
								backgroundForest1,			backgroundCemeteryBg3,		backgroundCemeteryBg2,
								backgroundCemeteryBg1 }
	end

	if place == "Arefu Fields" then
		backgroundElements =  { backgroundClouds1, 			backgroundMountain2,		backgroundMountain3,
								backgroundMountain1,		backgroundMountain5,		backgroundMountain4,		backgroundMountain6,
								backgroundForest1,			backgroundArefuFieldsBg3,	backgroundArefuFieldsBg2,
								backgroundArefuFieldsBg1 }
	end

	if place == "Arefu Cemetery01" then
		backgroundElements =  { backgroundClouds1, 			backgroundMountain2,		backgroundMountain3,
								backgroundMountain1,		backgroundMountain5,		backgroundMountain4,		backgroundMountain6,
								backgroundForest1,			backgroundCemeteryBg3,		backgroundCemeteryBg2,
								backgroundCemeteryBg1 }
	end

	if place == "Dungeon01" or place == "Dungeon02" then
		backgroundElements =  { backgroundDungeon01 }
	end
end

function createBackgroundAsset(x, y, speed, loop, autoscroll, autoscrollSpeed)
	local object = {
		x = x,
		y = y,
		speed = speed,
		loop = loop,
		autoscroll = autoscroll,
		autoscrollSpeed = autoscrollSpeed
	}

	return object
end

function loadBackgroundAssets()
	backgroundFog = {
		x		= 0,
		y		= 465,
		speed	= 50,
		loop	= 2, 		 -- [0] Don't loop at all! [1] Loop so there is one image at once. [2] Loop to fill whole camera [3] fill the camera
		autoscroll		= 1, -- [0] No scrolling. [1] Autoscroll if you stand still. [2] Autoscroll like a baws.
		autoscrollSpeed = 10
	}
	backgroundFog.image	= love.graphics.newImage("gfx/background/fog.png")
	backgroundFog.size	= backgroundFog.image:getWidth()
	--												      x,    y,  speed,    loop,  autoscroll, autoscrollSpeed
	-- ## GÓRY
	backgroundMountain1			= createBackgroundAsset( -300,	539,	0,		1,	0,	0 )
	backgroundMountain1.image	= love.graphics.newImage("gfx/background/mountain1.png")
	backgroundMountain1.size	= backgroundMountain1.image:getWidth()

	backgroundMountain2			= createBackgroundAsset( 0,		549,	0,		1,	0,	0 )
	backgroundMountain2.image	= love.graphics.newImage("gfx/background/mountain2.png")
	backgroundMountain2.size	= backgroundMountain2.image:getWidth()

	backgroundMountain3			= createBackgroundAsset( 200,	470,	0,		1,	0,	0 )
	backgroundMountain3.image	= love.graphics.newImage("gfx/background/mountain3.png")
	backgroundMountain3.size	= backgroundMountain3.image:getWidth()

	backgroundMountain4			= createBackgroundAsset( 300,	500,	15,		1,	0,	0 )
	backgroundMountain4.image	= love.graphics.newImage("gfx/background/mountain4.png")
	backgroundMountain4.size	= backgroundMountain4.image:getWidth()

	backgroundMountain5			= createBackgroundAsset( 0,	470,	15,		1,	0,	0 )
	backgroundMountain5.image	= love.graphics.newImage("gfx/background/mountain5.png")
	backgroundMountain5.size	= backgroundMountain5.image:getWidth()

	backgroundMountain6			= createBackgroundAsset( -20,	500,	15,		1,	0,	0 )
	backgroundMountain6.image	= love.graphics.newImage("gfx/background/mountain6.png")
	backgroundMountain6.size	= backgroundMountain6.image:getWidth()

	-- ## ZAMEK DRAKULI

	backgroundDraculaCastle			= createBackgroundAsset( 100,	640,	0.5,	0,	0,	0 )
	backgroundDraculaCastle.image	= love.graphics.newImage("gfx/background/dracula-castle.png")
	backgroundDraculaCastle.size	= backgroundDraculaCastle.image:getWidth()

	-- ## CHMURY

	backgroundClouds1				= createBackgroundAsset( 0,		705,	0,		0,	0,	0 )
	backgroundClouds1.image			= love.graphics.newImage("gfx/background/clouds1.png")
	backgroundClouds1.size			= backgroundClouds1.image:getWidth()

	-- ## TŁO ## AREFU FIELDS

	backgroundArefuFieldsBg1		= createBackgroundAsset( 0,		344,	55,		2,	0,	0 )
	backgroundArefuFieldsBg1.image	= love.graphics.newImage("gfx/background/arefu-fields-bg1.png")
	backgroundArefuFieldsBg1.size	= backgroundArefuFieldsBg1.image:getWidth()

	backgroundArefuFieldsBg2		= createBackgroundAsset( 200,	350,	40,		2,	0,	0 )
	backgroundArefuFieldsBg2.image	= love.graphics.newImage("gfx/background/arefu-fields-bg2.png")
	backgroundArefuFieldsBg2.size	= backgroundArefuFieldsBg2.image:getWidth()

	backgroundArefuFieldsBg3		= createBackgroundAsset( 500,	356,	30,		2,	0,	0 )
	backgroundArefuFieldsBg3.image	= love.graphics.newImage("gfx/background/arefu-fields-bg3.png")
	backgroundArefuFieldsBg3.size	= backgroundArefuFieldsBg3.image:getWidth()

	-- ## TŁO ## CMENTARZ

	backgroundCemeteryBg1		= createBackgroundAsset( 0,		344,	55,		2,	0,	0 )
	backgroundCemeteryBg1.image	= love.graphics.newImage("gfx/background/cemetery-bg1.png")
	backgroundCemeteryBg1.size	= backgroundCemeteryBg1.image:getWidth()

	backgroundCemeteryBg2		= createBackgroundAsset( 200,	350,	40,		2,	0,	0 )
	backgroundCemeteryBg2.image	= love.graphics.newImage("gfx/background/cemetery-bg2.png")
	backgroundCemeteryBg2.size	= backgroundCemeteryBg2.image:getWidth()

	backgroundCemeteryBg3		= createBackgroundAsset( 500,	356,	30,		2,	0,	0 )
	backgroundCemeteryBg3.image	= love.graphics.newImage("gfx/background/cemetery-bg3.png")
	backgroundCemeteryBg3.size	= backgroundCemeteryBg3.image:getWidth()

	-- ## LAS

	backgroundForest1				= createBackgroundAsset( 0, 398, 20, 2, 0, 0 )
	backgroundForest1.image			= love.graphics.newImage("gfx/background/forest1.png")
	backgroundForest1.size			= backgroundForest1.image:getWidth()

	-- ## TŁO ### DUNGEON01

	backgroundDungeon01				= createBackgroundAsset( 0, 0, 0, 3, 0, 0 )
	backgroundDungeon01.image		= love.graphics.newImage("gfx/background/dungeon01.png")
	backgroundDungeon01.size		= backgroundDungeon01.image:getWidth()
end