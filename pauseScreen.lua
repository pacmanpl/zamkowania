pauseScreenState = "overview" -- overview/equip/upgrade
pauseScreenPosition = 1 -- menu position that is currently selected
upgradeCost = {50,75,100,150,200,300,500}

function loadPauseScreen()
	pauseScreen = love.graphics.newImage("gfx/pause-screen.png")
	overviewEquipSelected 	= love.graphics.newImage("gfx/pause-menu/overview-equip-selected.png")
	overviewEquip 			= love.graphics.newImage("gfx/pause-menu/overview-equip.png")
	overviewUpgradeSelected = love.graphics.newImage("gfx/pause-menu/overview-upgrade-selected.png")
	overviewUpgrade 		= love.graphics.newImage("gfx/pause-menu/overview-upgrade.png")

	equipDagger					= love.graphics.newImage("gfx/pause-menu/equip-dagger.png")
	equipDaggerSelected			= love.graphics.newImage("gfx/pause-menu/equip-dagger-selected.png")
	equipDaggerEmpty			= love.graphics.newImage("gfx/pause-menu/equip-dagger-empty.png")
	equipDaggerEmptySelected	= love.graphics.newImage("gfx/pause-menu/equip-dagger-empty-selected.png")

	equipAxe					= love.graphics.newImage("gfx/pause-menu/equip-axe.png")
	equipAxeSelected			= love.graphics.newImage("gfx/pause-menu/equip-axe-selected.png")
	equipAxeEmpty				= love.graphics.newImage("gfx/pause-menu/equip-axe-empty.png")
	equipAxeEmptySelected		= love.graphics.newImage("gfx/pause-menu/equip-axe-empty-selected.png")

	equipHoly					= love.graphics.newImage("gfx/pause-menu/equip-holy.png")
	equipHolySelected			= love.graphics.newImage("gfx/pause-menu/equip-holy-selected.png")
	equipHolyEmpty				= love.graphics.newImage("gfx/pause-menu/equip-holy-empty.png")
	equipHolyEmptySelected		= love.graphics.newImage("gfx/pause-menu/equip-holy-empty-selected.png")

	equipCross					= love.graphics.newImage("gfx/pause-menu/equip-cross.png")
	equipCrossSelected			= love.graphics.newImage("gfx/pause-menu/equip-cross-selected.png")
	equipCrossEmpty				= love.graphics.newImage("gfx/pause-menu/equip-cross-empty.png")
	equipCrossEmptySelected		= love.graphics.newImage("gfx/pause-menu/equip-cross-empty-selected.png")

	equipMark = love.graphics.newImage("gfx/pause-menu/equip-mark.png")

	upgradeWhip			= love.graphics.newImage("gfx/pause-menu/upgrade-whip.png")
	upgradeWhipSelected	= love.graphics.newImage("gfx/pause-menu/upgrade-whip-selected.png")
	upgradeSubweapon			= love.graphics.newImage("gfx/pause-menu/upgrade-subweapon.png")
	upgradeSubweaponSelected	= love.graphics.newImage("gfx/pause-menu/upgrade-subweapon-selected.png")
	upgradeHealth			= love.graphics.newImage("gfx/pause-menu/upgrade-health.png")
	upgradeHealthSelected	= love.graphics.newImage("gfx/pause-menu/upgrade-health-selected.png")

	upgradeLines = love.graphics.newImage("gfx/pause-menu/upgrade-lines.png")

	healthBar		= love.graphics.newImage("gfx/pause-menu/health-bar.png")
	healthBarEmpty	= love.graphics.newImage("gfx/pause-menu/health-bar-empty.png")
end

function drawPauseScreen()
	love.graphics.draw(pauseScreen, 0, 0)
	drawPauseMenu()
	drawHealth()
	love.graphics.setColor(223, 0, 2)
	love.graphics.printf(player.hearts, 224, 178, 45, "center")
	love.graphics.setColor(255, 255, 255)
end

function drawHealth()
	for i=1, player.maxHealth, 1 do
		if i <= player.health then
			love.graphics.draw(healthBar, 131 + (i-1) * 15, 40)
		else
			love.graphics.draw(healthBarEmpty, 131 + (i-1) * 15, 40)
		end
	end
end

function drawHeartCount()

end

function drawPauseMenu()
	if pauseScreenState == "overview" then
		if pauseScreenPosition == 1 	then	love.graphics.draw(overviewEquipSelected, 0, 131)
										else	love.graphics.draw(overviewEquip, 0, 131)
										end
		if pauseScreenPosition == 2 	then	love.graphics.draw(overviewUpgradeSelected, 0, 181)
										else	love.graphics.draw(overviewUpgrade, 0, 181)
										end
	end

	if pauseScreenState == "equip" then
		-- draw Dagger
		if player.hasDagger then
			if pauseScreenPosition == 1		then	love.graphics.draw(equipDaggerSelected, 0, 131)
											else	love.graphics.draw(equipDagger, 0, 131)
											end
		else
			if pauseScreenPosition == 1		then	love.graphics.draw(equipDaggerEmptySelected, 0, 131)
											else	love.graphics.draw(equipDaggerEmpty, 0, 131)
											end
		end

		-- draw Axe

		if player.hasAxe then
			if pauseScreenPosition == 2		then	love.graphics.draw(equipAxeSelected, 0, 171)
											else	love.graphics.draw(equipAxe, 0, 171)
											end
		else
			if pauseScreenPosition == 2		then	love.graphics.draw(equipAxeEmptySelected, 0, 171)
											else	love.graphics.draw(equipAxeEmpty, 0, 171)
											end
		end

		-- draw Holy Water

		if player.hasHoly then
			if pauseScreenPosition == 3		then	love.graphics.draw(equipHolySelected, 0, 202)
											else	love.graphics.draw(equipHoly, 0, 202)
											end
		else
			if pauseScreenPosition == 3		then	love.graphics.draw(equipHolyEmptySelected, 0, 202)
											else	love.graphics.draw(equipHolyEmpty, 0, 202)
											end
		end

		-- draw Cross

		if player.hasCross then
			if pauseScreenPosition == 4		then	love.graphics.draw(equipCrossSelected, 0, 236)
											else	love.graphics.draw(equipCross, 0, 236)
											end
		else
			if pauseScreenPosition == 4		then	love.graphics.draw(equipCrossEmptySelected, 0, 236)
											else	love.graphics.draw(equipCrossEmpty, 0, 236)
											end
		end

		-- draw equip marker

		local yOffset
		if player.subweapon == "dagger" then yOffset = 11 end
		if player.subweapon == "axe" then yOffset = 44 end
		if player.subweapon == "holy" then yOffset = 77 end
		if player.subweapon == "cross" then yOffset = 110 end
		love.graphics.draw(equipMark, 73, 131 + yOffset)
	end

	if pauseScreenState == "upgrade" then
		-- draw upgrade whip
		if pauseScreenPosition == 1 then love.graphics.draw(upgradeWhipSelected, 0, 131)
									else love.graphics.draw(upgradeWhip, 0, 131)
									end
		love.graphics.setColor(255,134,65,200)
		love.graphics.print(player.whipLevel, 72, 136)
		if player.hearts >= upgradeCost[player.whipLevel] then
			love.graphics.setColor(255,255,255,200)
		else
			love.graphics.setColor(223,0,2,200)
		end
		love.graphics.printf(upgradeCost[player.whipLevel], 114, 136, 36, "center")
		love.graphics.setColor(255,255,255,255)

		-- draw upgrade subweapon
		if pauseScreenPosition == 2 then love.graphics.draw(upgradeSubweaponSelected, 0, 176)
									else love.graphics.draw(upgradeSubweapon, 0, 176)
									end
		love.graphics.setColor(255,134,65,200)
		love.graphics.print(player.subweaponLevel, 72, 181)
		if player.hearts >= upgradeCost[player.subweaponLevel] then
			love.graphics.setColor(255,255,255,200)
		else
			love.graphics.setColor(223,0,2,200)
		end
		love.graphics.printf(upgradeCost[player.subweaponLevel], 114, 181, 36, "center")
		love.graphics.setColor(255,255,255,255)
		
		-- draw upgrade subweapon
		if pauseScreenPosition == 3 then love.graphics.draw(upgradeHealthSelected, 0, 225)
									else love.graphics.draw(upgradeHealth, 0, 225)
									end
		love.graphics.setColor(255,134,65,200)
		love.graphics.print(player.healthLevel, 72, 230)
		if player.hearts >= upgradeCost[player.healthLevel] then
			love.graphics.setColor(255,255,255,200)
		else
			love.graphics.setColor(223,0,2,200)
		end
		love.graphics.printf(upgradeCost[player.healthLevel], 114, 230, 36, "center")
		love.graphics.setColor(255,255,255,255)

		-- draw fancy lines from heart counter to upgrades
		love.graphics.draw(upgradeLines, 102, 130)
	end
end

function pauseScreenKeypressed( key, unicode )
	if gameState == "paused" then
		local maxPosition
		if pauseScreenState == "overview" then maxPosition = 2 end
		if pauseScreenState == "equip" then maxPosition = 4 end
		if pauseScreenState == "upgrade" then maxPosition = 3 end

		if key == "w" then
			-- move position up
			if pauseScreenPosition == 1 then
				pauseScreenPosition = maxPosition
			else
				pauseScreenPosition = pauseScreenPosition - 1
			end
		elseif key == "s" then
			-- move position down
			if pauseScreenPosition == maxPosition then
				pauseScreenPosition = 1
			else
				pauseScreenPosition = pauseScreenPosition + 1
			end
		elseif key == "o" then
			-- canceling menus
			if pauseScreenState == "equip" then
				pauseScreenState = "overview"
				pauseScreenPosition = 1
			elseif pauseScreenState == "upgrade" then
				pauseScreenState = "overview"
				pauseScreenPosition = 2
			elseif pauseScreenState == "overview" then
				gameState = "playing"
				pauseScreenPosition = 1
			end
		elseif key == "p" then
			-- moving through menus and accepting
			if pauseScreenState == "overview" then
				if pauseScreenPosition == 1 then pauseScreenState = "equip" end
				if pauseScreenPosition == 2 then
					pauseScreenState = "upgrade"
					pauseScreenPosition = 1
				end
			end
		end
	end
end