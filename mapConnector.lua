--local goIcon = love.graphics.newImage("gfx/arrow-small.png")
local enterPopUp = love.graphics.newImage("gfx/enter-pop-up.png")

--local g = anim8.newGrid(30, 29, goIcon:getWidth(), goIcon:getHeight())
--
--animGoIconRigth	= anim8.newAnimation('loop', g('1-3,1'), 0.5)
--animGoIconLeft	= anim8.newAnimation('loop', g('1-3,1'), 0.5, nil, true)

local g = anim8.newGrid(80, 26, enterPopUp:getWidth(), enterPopUp:getHeight())

animEnterPopUp = anim8.newAnimation(g('1-7',1), {[1]=0.1,[2]=0.12,[3]=0.14,[4]=0.16,[5]=0.14,[6]=0.12,[7]=0.1})

transitionTime = 0 -- time of blackout betwen changing maps

function updateMapConnector(dt, x, y, currentPlace, map)
	--animGoIconRigth:update(dt)
	--animGoIconLeft:update(dt)
	animEnterPopUp:update(dt)

	if currentPlace == "Arefu" then
		--if x <= 16 * 15 then
		--	teleportPlayer(16 * 15, 472, "Arefu Fields", map02)
		--end
		if x <= 16 * 13 then
			teleportPlayer(16 * 156, 472, "Arefu Fields", map02)
		end
		if x >= 16 * 160 then
			teleportPlayer(16 * 17, 472, "Arefu Cemetery01", map04)
		end

		-- # D O O R S
		--if x > 16 * 58 and x < 16 * 60 and
		--	y > 16 * 15 and y < 16 * 20 then
		--	player.nearestDoor = {16 * 3, 16 * 15, "Arefu - Inn", map03, 16 * 60, 16 * 16}
		--else
		--	player.nearestDoor = nil
		--end
	end
	--if player.x > 16 * 58 and player.x < 16 * 60 and dialogState == nil then
	--	player.dialogState = "doors"
	--end

	if currentPlace == "Arefu - Inn" then
		if x <= 16 * 2 then
			teleportPlayer(16 * 144, 16 * 17, "Arefu", map01)
		end
	end

	if currentPlace == "Arefu Fields" then
		if x <= 16 * 13 then
			teleportPlayer(16 * 156, 472, "Arefu Cemetery01", map04)
		end
		if x >= 16 * 160 then
			teleportPlayer(16 * 17, 472, "Arefu", map01)
		end
	end

	if currentPlace == "Arefu Cemetery01" then
		if x <= 16 * 13 then
			teleportPlayer(16 * 156, 472, "Arefu", map01)
		end
		if x >= 16 * 160 then
			teleportPlayer(16 * 15, 472, "Arefu Fields", map02)
		end

		-- # D O O R S
		if x > 16 * 122 and x < 16 * 125 and
			y > 16 * 39 and y < 16 * 41 then
			player.nearestDoor = {16 * 11, 16 * 10, "Dungeon01", map05, 16 * 125, 16 * 39}
		else
			player.nearestDoor = nil
		end
	end

	if currentPlace == "Dungeon01" then
		if x >= 16 * 106 and y > 16 * 89 then
			teleportPlayer(16 * 7, 16 * 83, "Dungeon02", map06)
		end
		if x >= 16 * 106 and y < 16 * 10 then
			teleportPlayer(16 * 7, 16 * 8, "Dungeon02", map06)
		end
		-- # D O O R S
		if x > 16 * 10 and x < 16 * 12 and
			y > 16 * 9 and y < 16 * 12 then
			player.nearestDoor = {16 * 123, 16 * 40, "Arefu Cemetery01", map04, 16 * 12, 16 * 9}
		else
			player.nearestDoor = nil
		end
	end

	if currentPlace == "Dungeon02" then
		if x <= 16 * 3 and y > 16 * 79 then
			teleportPlayer(16 * 103, 16 * 92, "Dungeon01", map05)
		end
		if x <= 16 * 3 and y < 16 * 10 then
			teleportPlayer(16 * 103, 16 * 8, "Dungeon01", map05)
		end
	end
end

function teleportPlayer(x, y, place, map)
	currentMap = map
	currentPlace = place

	player.x = x
	player.y = y
	camX = player.x - (windowWidth / 2)
	if camX < 0 then camX = 0 end
	camY = player.y - (windowHeight / 2)
	if camY < 0 then camY = 0 end
	entities = {}
	particles = {}
	loadSpawnPoints(currentPlace)
	loadMapElements(currentPlace)
	loadBackgroundTable(place)
	camera:setBounds( 0, 0, (currentMap.width * currentMap.tileWidth) - windowWidth, (currentMap.height * currentMap.tileHeight) - windowHeight )

	mapTiles = {}

	tiles = love.filesystem.getDirectoryItems( "maps/"..currentPlace )

	for k, file in ipairs(tiles) do
		-- Example of file names: "11.png" "12.png" "13.png" "21.png"

		-- Extrack first two numbers from filename
		local s1, s2
		s1 = tonumber(string.sub( file, 1, 1 ))
		s2 = tonumber(string.sub( file, 2, 2 ))

		if s2 == 1 then
			mapTiles[s1] = {}
		end

		-- Create an image of a level tile
		mapTiles[s1][s2] = love.graphics.newImage("maps/"..currentPlace.."/"..file)
	end

	mapTransition()
end

function mapTransition()
	transitionTime = 0.39
	gameState = "mapTransition"
	love.graphics.setBackgroundColor(0,0,0)
end

function updateMapTransition(dt)
	transitionTime = transitionTime - dt
	if transitionTime <= 0 then
		transitionTime = 0
		love.graphics.setBackgroundColor(68,72,96)
		gameState = "playing"
	end
end

function drawMapConnector()
	--if player.x < 16 * 20 then
	--	animEnterPopUp:draw(enterPopUp, math.floor(player.x - 100), 432)
	--end
	--if player.x > 16 * 58 and player.x < 16 * 60 and player.dialogState == "doors" then
	--	animEnterPopUp:draw(enterPopUp, 16 * 60, 16 * 17)
	--end
	-- jeśli stoi przy drzwiach, wyświetl ikonkę
	if player.nearestDoor ~= nil then
		animEnterPopUp:draw(enterPopUp, player.nearestDoor[5], player.nearestDoor[6])
	end
end

function mapConnectorKeypressed( key, unicode )
	if key == "o" and player.nearestDoor ~= nil then
		teleportPlayer(player.nearestDoor[1], player.nearestDoor[2], player.nearestDoor[3], player.nearestDoor[4])
		player.nearestDoor = nil
	end
end