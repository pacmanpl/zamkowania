anim8 = require "anim8"

require "camera"
require "player/player"
require "titleScreen"
require "pauseScreen"
require "backgroundTerrain"
require "spawns"
require "mapElements"
require "particles"
require "maps/platform/platform"
require "maps/arrowLauncher/arrowLauncher"
require "NPCs/villager"
require "NPCs/innkeeper"
require "enemies/skeleton/skeleton"
require "enemies/wolf/wolf"
require "enemies/scarecrow/scarecrow"
require "enemies/bat/bat"
require "enemies/maggot/maggot"
require "enemies/ghost/ghost"
require "enemies/skeletonSwordsman/skeletonSwordsman"
require "enemies/ulgan/ulgan"
require "dialogs"
require "mapConnector"
require "objects/small-heart"
require "objects/meat-ball"
require "objects/launcherArrow"

gameState = "titleScreen"
scale = 2
windowWidth		= 400
windowHeight	= 300

function love.load()
	-- debug file
	file = love.filesystem.newFile('debug.txt')
	file:open('a')

	load_resources()
end

function love.update(dt)
	--[[
	Po ostatnim hicie player opada na ziemię - timer leci kiedy player.isDying
	Jeśli player.isDying i timer = 0 --> gameState = "gameOverScreen" --> Czarny ekran + pojawiający się YOU ARE DEAD
	Masz zapisane X, Y, nazwę mapy, na której umarłeś i na której się zrespawnujesz
	Press Enter to continue
	gameOverScreen fade out z timerem i po timerze gameState = playingm
	]]
	if dt > 0.05 then dt = 0.005 end
	if love.keyboard.isDown( "s" ) then
		dt = dt / 20
	end

	-- if player is dead slow the game
	if player.status == "dead" then dt = dt / 2 end
	if gameState == "playing" then
		-- move the background
		updateBackground(dt, player.x)
		-- reset player x/y
		if love.keyboard.isDown( "z" ) then
			player:spawnPlayer(200, 456)
		end
		-- if something goes REALLY wrong, restart player Y coord (USELESS 8D)
		if player.y > 1600 then player.y = 898 end
		-- update player position
		player:update(dt, gravity, currentMap)
		-- update entities
		for k, entity in pairs( entities ) do
			entity:update(dt, k)
		end
		-- update map elements
		player.onPlatform2 = false
		for k, element in pairs( mapElements ) do
			element:update(dt)
		end
		updateBossWall(dt)

		if not player.onPlatform2 then player.onPlatform = false end

		for k, particle in pairs( particles ) do
			particle:update(dt, k)
		end
		
		-- update hud animation
		animHudSouls:update(dt)
		updateHudSouls(dt)
		-- update dialog boxes
		updateDialog(dt)
		-- update level (spawn entities)
		updateSpawns(dt, currentMap)
		-- move player to different locations
		updateMapConnector(dt, player.x, player.y, currentPlace, currentMap)

		-- move the camera
		updateCamera(dt)
	end
	if gameState == "mapTransition" then
		updateMapTransition(dt)
	end
end

function updateHudSouls(dt)
	if player.soulBufferTimer > 0 then player.soulBufferTimer = player.soulBufferTimer - dt end
	if player.soulBuffer > 0 and player.soulBufferTimer <= 0 then
		local fasts
		if player.soulBuffer <= 10000 then fasts = 600 end
		if player.soulBuffer <= 5000 then fasts = 500 end
		if player.soulBuffer <= 2000 then fasts = 450 end
		if player.soulBuffer <= 1000 then fasts = 350 end
		if player.soulBuffer <= 500  then fasts = 250 end
		if player.soulBuffer <= 100  then fasts = 30 end
		if player.soulBuffer <= 10   then fasts = 10 end

		local transfer = fasts * dt
		player.soulBuffer = player.soulBuffer - transfer
		player.souls = player.souls + transfer
	end
end

function love.draw()
	love.graphics.scale(scale, scale)
	if gameState == "titleScreen" then
		drawTitleScreen()
	elseif gameState == "playing" then
		camera:set()

		drawBackground()

		currentMap:setDrawRange(camX, camY, windowWidth, windowHeight + 32)
		currentMap:draw()
		if next(mapTiles) ~= nil then
			if gameState == "playing" then

			local x1,y1, x2,y2, x3,y3, x4,y4
			x1 = math.floor(camX / 512) + 1
			y1 = math.floor(camY / 512) + 1
			dupa1 = x1
			dupa2 = y1
							--file:open('a')
							--file:write(camX)
							--file:close()
				love.graphics.draw(mapTiles[x1][y1], (x1 - 1) * 512, (y1 - 1) * 512) end

			x2 = math.floor((camX + windowWidth) / 512) + 1
			y2 = math.floor(camY / 512) + 1
			if not (x2 == x1 and y2 == y1) then
				love.graphics.draw(mapTiles[x2][y2], (x2 - 1) * 512, (y2 - 1) * 512)
			end

			x3 = math.floor(camX / 512) + 1
			y3 = math.floor((camY + windowHeight) / 512) + 1
			if not (x3 == x1 and y3 == y1) then
				if not (x3 == x2 and y3 == y2) then
					love.graphics.draw(mapTiles[x3][y3], (x3 - 1) * 512, (y3 - 1) * 512)
				end
			end

			x4 = math.floor((camX + windowWidth) / 512) + 1
			y4 = math.floor((camY + windowHeight) / 512) + 1
			if not (x4 == x1 and y4 == y1) then
				if not (x4 == x2 and y4 == y2) then
					if not (x4 == x3 and y4 == y3) then
						love.graphics.draw(mapTiles[x4][y4], (x4 - 1) * 512, (y4 - 1) * 512)
					end
				end
			end
		end

		for k, entity in pairs( entities ) do
			entity:draw()
		end

		for k, element in pairs( mapElements ) do
			element:draw()
		end
		drawBossWall()

		for k, particle in pairs( particles ) do
			particle:draw()
		end
		
		player:drawPlayer()

		--love.graphics.setBlendMode("additive")
		--love.graphics.draw(p, player.x, player.y)
		--love.graphics.setBlendMode("alpha")

		drawMapConnector()

		drawDialog()

		camera:unset()

		drawHUD()

		FPS = love.timer.getFPS()

		--if player.onPlatform2 then love.graphics.print("onPlatform = true", 5, 20) else love.graphics.print("onPlatform = false", 5, 20) end
		--if player.onFloor then love.graphics.print("onFloor = true", 5, 40) else love.graphics.print("onFloor = false", 5, 40) end
		--if player.isJumping then love.graphics.print("isJumping = true", 5, 60) else love.graphics.print("isJumping = false", 5, 60) end
		--if player.onStairs then love.graphics.print("onStairs = true", 5, 80) else love.graphics.print("onStairs = false", 5, 80) end
		--love.graphics.print("y = ".. math.round(camY) + 400 - math.round(player.y), 5, 100)
		--love.graphics.print("y = "..player.yVelocity, 5, 115)
		--love.graphics.print("yVelocity = "..player.yVelocity, 5, 130)
		--love.graphics.print("FPS = "..FPS, 5, 145)

		--love.graphics.print("Diff = "..camY - (player.y - 180), 5, 120)
		--love.graphics.print("Tile = "..math.ceil(player.x / 16), 5, 100)
		--if dupa1 ~= nil then love.graphics.print(dupa1.." "..dupa2, 5, 140) end
		--if entities[1] ~= nil then love.graphics.print("debug:   "..entities[1].debug, 5, 5) end
		--if entities[1] ~= nil then love.graphics.print("direction:   "..entities[1].direction, 5, 25) end
		--if entities[1] ~= nil then love.graphics.print("xVelocity: "..entities[1].xVelocity, 5, 50) end
		--if entities[1] ~= nil then love.graphics.print("stepCount:  "..entities[1].stepCount, 5, 75) end
		--if entities[1] ~= nil then love.graphics.print("patterntimer: "..entities[1].patternTimer, 5, 100) end

		--love.graphics.print("[1/q]EmissionRate = "..EmissionRate, 5, 20)
		--love.graphics.print("[2/x]ParticleLifetime = "..ParticleLifetime, 5, 30)
		--love.graphics.print("[3/c]Spread = "..Spread, 5, 40)
		--love.graphics.print("[4/v]RadialAcceleration = "..RadialAcceleration, 5, 50)
		--love.graphics.print("[5/b]SizeVariation = "..SizeVariation, 5, 60)
		--love.graphics.print("[6/n]AreaSpread = "..AreaSpread, 5, 70)

	elseif gameState == "paused" then
		drawPauseScreen()
	elseif gameState == "gameOverScreen" then
		love.graphics.print('G A M E  O V E R', windowWidth / 2 - 30, windowHeight / 2)
	end
end

function drawHUD()
	love.graphics.draw(hudHead, 9, 9)
	for i=1, player.maxHealth, 1 do
		if i <= player.health then
			love.graphics.draw(hudHealthFull, 40 + (i-1) * 6, 16)
		else
			love.graphics.draw(hudHealthEmpty, 40 + (i-1) * 6, 16)
		end
	end

	animHudSouls:draw(hudSouls, 13, 266)
	love.graphics.setColor(0,0,0)
	love.graphics.printf(math.round(player.souls), 36, 273, 30, "left")
	love.graphics.setColor(255,212,72)
	love.graphics.printf(math.round(player.souls), 35, 272, 30, "left")
	love.graphics.setColor(255,255,255)

	if player.soulBuffer > 0 then
		love.graphics.setColor(0,0,0, 150)
		love.graphics.printf(" +"..math.round(player.soulBuffer), 36, 259, 30, "left")
		love.graphics.setColor(255,212,72, 150)
		love.graphics.printf(" +"..math.round(player.soulBuffer), 35, 258, 30, "left")
		love.graphics.setColor(255,255,255, 255)
	end

	love.graphics.draw(hudHearts, 369, 16)
	love.graphics.setColor(0,0,0)
	love.graphics.printf(player.hearts, 335, 21, 30, "right")
	love.graphics.setColor(158,0,2)
	love.graphics.printf(player.hearts, 334, 20, 30, "right")
	love.graphics.setColor(255,255,255)
end

function load_resources()
	love.window.setMode(windowWidth * scale, windowHeight * scale)
	-- for full screen
	--love.window.setMode(windowWidth * scale, windowHeight * scale, {fullscreen=true})

	unblurredFont = love.graphics.newImage("gfx/font.png")
	font = love.graphics.newImageFont(unblurredFont,
		" abcdefghijklmnopqrstuvwxyz" ..
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
		"123456789.,!?-+/():;%&`'*#=[]\"")
	love.graphics.setFont(font)

	-- HUD
	hudHead = love.graphics.newImage("gfx/hud-head.png")
	hudHealthFull = love.graphics.newImage("gfx/hud-health-full.png")
	hudHealthEmpty = love.graphics.newImage("gfx/hud-health-empty.png")
	hudHearts = love.graphics.newImage("gfx/hud-hearts.png")
	hudSouls = love.graphics.newImage("gfx/hud-souls.png")
	local g = anim8.newGrid(17, 28, hudSouls:getWidth(), hudSouls:getHeight())
	animHudSouls = anim8.newAnimation(g('1-8',1), 0.15)

	-- Load music and sounds
	loadSound()

	-- Load maps & stuff
	local ATL = require("AdvTiledLoader")
	ATL.Loader.path = 'maps/'

	map01 = ATL.Loader.load('map01.tmx')
	map02 = ATL.Loader.load('map02.tmx')
	map03 = ATL.Loader.load('map03.tmx')
	map04 = ATL.Loader.load('map04.tmx')
	map05 = ATL.Loader.load('map05.tmx')
	map06 = ATL.Loader.load('map06.tmx')

	currentMap = map06
	currentPlace = "Dungeon02"
	lastSpawnPosition = {1000,456} -- for respawning after death

	startingPositionX = 50
	startingPositionY = 38

	--love.graphics.setBackgroundColor(71,31,116)
	love.graphics.setBackgroundColor(68,72,96)

	camera:setBounds( 0, 0, (currentMap.width * currentMap.tileWidth) - windowWidth + 1, currentMap.height * currentMap.tileHeight - windowHeight )
	camX = 1000
	camY = 456

	-- particles
	particleImage01 = love.graphics.newImage("gfx/particle-soul.png")

	player = Player:new()

	player.width = 32
	player.height = 64
	player.walkSpeed = 125
	player.jumpSpeed = -320
	hasJumped = false

	gravity = 750

	-- Load pause screen images
	loadPauseScreen()

	-- Load dialog images
	loadDialog()

	-- Load start map spawns | uses images from loadDialog()
	entities = {}		-- Array for every spawned NPC/enemy/bullet
	mapElements = loadMapElements(currentPlace)	-- Array for every moving platform, spikes, traps
	spawnPoints = loadSpawnPoints(currentPlace)
	mapTiles = {} -- Array with images of map
	particles = {}

	-- Load backgroundTerrain gfx
	loadBackgroundAssets()
	loadBackgroundTable(currentPlace)

	-- Load various gfx
	bossWall  = love.graphics.newImage("gfx/boss-wall.png")
	bossWall2 = love.graphics.newImage("gfx/boss-wall2.png")

	-- tests
			--EmissionRate			= 80		--1
			--ParticleLifetime		= 1			--2
			--Spread					= 0		--3
			--RadialAcceleration		= 0			--4
			--SizeVariation			= 0.4		--5
			--AreaSpread				= 4		--6

	--p = love.graphics.newParticleSystem(particleImage01, 80)
	--p:setEmissionRate          (EmissionRate)
 	--p:setEmitterLifetime       (1)
 	--p:setParticleLifetime      (ParticleLifetime)
 	--p:setPosition              (0, 0)
 	--p:setDirection             (0)
 	--p:setSpread                (Spread)
 	--p:setSpeed                 (0, 1)
 	--p:setRadialAcceleration    (RadialAcceleration)
 	--p:setSizes                 (0.5, 1, 2)
 	--p:setSizeVariation         (SizeVariation)
 	--p:setRotation              (0)
 	--p:setSpin                  (0)
 	--p:setLinearAcceleration    (0, -20, 0, -50)
 	--p:setColors                (89, 124, 15, 240, 203, 245, 114, 240, 251, 175, 93, 200, 155, 86, 12, 50)
 	--p:setTangentialAcceleration(0,0)
 	--p:setRelativeRotation	   (true)
 	--p:setAreaSpread            ('normal', 6, 14)
	--p:start()
end

function loadSound()
	sourcesCount = 1
	sources = {}
	sfx = {}
	sfx.WhipHit		= love.sound.newSoundData("sfx/whipHit.mp3")
	sfx.WhipUse		= love.sound.newSoundData("sfx/whipUse.mp3")
	sfx.HitGround	= love.sound.newSoundData("sfx/hitGround.mp3")
	sfx.DamageTaken	= love.sound.newSoundData("sfx/damageTaken.mp3")
	sfx.PlayerDeath	= love.sound.newSoundData("sfx/playerDeath.mp3")
	sfx.EnemyDeath	= love.sound.newSoundData("sfx/enemyDeath.mp3")
	sfx.Confirm		= love.sound.newSoundData("sfx/confirm.mp3")
	sfx.CollectSoul	= love.sound.newSoundData("sfx/collectSoul.mp3")
end

function playSound(sound)
	local source = love.audio.newSource(sfx[sound])
	sources[sourcesCount] = source
	love.audio.play(sources[sourcesCount])
	sourcesCount = sourcesCount + 1
	if sourcesCount == 17 then sourcesCount = 1 end
end

function love.keypressed(key, unicode)
	if player.status == "alive" then player:keypressed(key, unicode) end
	-- keypressed for every entity
	for k, entity in pairs( entities ) do
		entity:keypressed(key, unicode)
	end

	-- keypressed on dialogs
	dialogKeypressed(key, unicode)
	-- keypressed on pause screen
	pauseScreenKeypressed(key, unicode)

	-- keypressed on teleporting (doors)
	mapConnectorKeypressed(key, unicode)

	-- ENTER
	if key == "return" then
		if gameState == "paused" then
			gameState = "playing"
			pauseScreenState = "overview"
			pauseScreenPosition = 1
		elseif gameState == "titleScreen" then
			-- arefu inn spawn player:spawnPlayer(18 * 16, 15 * 16)
			--player:spawnPlayer(startingPositionX * 16, startingPositionY * 40)
			teleportPlayer(startingPositionX * 16, startingPositionY * 16, currentPlace, currentMap)
			gameState = "playing"
		elseif gameState == "playing" then
			gameState = "paused"
		elseif gameState == "gameOverScreen" then
			gameState = "titleScreen"
			-- put here spawning player in last position, cuting hearts in half and so on
			player.status = "alive"
			player.health = 4
			player:spawnPlayer(1000, 456)
		end
	end
	if key == ("escape") then
		if gameState == "playing" then
			gameState = "paused" 
		elseif gameState == "paused" then
			gameState = "playing"
		elseif gameState == "titleScreen" then
			file:close()
			love.event.quit()
		end
	end
	-- key for title screen Try to figure out how to go to title screen
	if key == ("]") then gameState = "titleScreen" end

	-- cheaaaaats
	--if key == "6" then player.soulBuffer = player.soulBuffer + 150 end
	if key == "6" then player.x = player.x + 150 end
	if key == "4" then player.x = player.x - 150 end
	if key == "8" then player.y = player.y - 150 end
	if key == "2" then player.y = player.y + 150 end

	if key == "b" then createBossWall(6, 6, 14, 5) end
	if key == "n" then removeBossWall() end
end

function love.keyreleased(key)
	-- stop the player if movement key is realeased ("if" is to prevent reseting animation)
	if (key == "d" or key == "a" or key == "w" or key == "s") and player.onFloor and player.status == "alive" then
		player:stop()
		if (not love.keyboard.isDown( "a" )) and (not love.keyboard.isDown( "d" )) then
			playerAnimation:gotoFrame(1)
		end
	end
	-- reset animation when you climb stairs
	if key == "w" and player.onStairs then
		animWalkUpstairsRight:gotoFrame(7)
		animWalkUpstairsLeft:gotoFrame(7)
	end
	if key == "s" and player.onStairs then
		animWalkDownstairsRight:gotoFrame(1)
		animWalkDownstairsLeft:gotoFrame(1)
	end
	if (key == "w" or key == "s") and player.onStairs then
		playerAnimation:pause();
	end

	-- spawn heart
	--if key == "f" then
	--	local sx = player.x + 40
	--	local sy = player.y
	--	--table.insert( entities, smallHeart:new(sx,sy))
	--	local xSpeed, ySpeed
	--	table.insert(entities, meatBall:new(player.x+30, player.y, 50, -400, 800, 0))
	--end
end

function love.focus(f)
	if not f and gameState == "playing" then
		gameState = "paused"
	end
end

function math.clamp(x, min, max)
	return x < min and min or (x > max and max or x)
end

function math.round(x)
	return math.floor(x + 0.5)
end

function isColliding(map, x, y)
	local tileX, tileY = math.floor(x / map.tileWidth), math.floor(y / map.tileHeight)
	local tile = map("walls")(tileX, tileY)

	return not(tile == nil)
end