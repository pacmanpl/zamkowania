function loadMapElements(currentPlace)
	mapElements = {}
	--[[

	 ██████╗  ██╗        █████╗ ██████╗ ███████╗███████╗██╗   ██╗
	██╔═████╗███║       ██╔══██╗██╔══██╗██╔════╝██╔════╝██║   ██║
	██║██╔██║╚██║       ███████║██████╔╝█████╗  █████╗  ██║   ██║
	████╔╝██║ ██║       ██╔══██║██╔══██╗██╔══╝  ██╔══╝  ██║   ██║
	╚██████╔╝ ██║██╗    ██║  ██║██║  ██║███████╗██║     ╚██████╔╝
	 ╚═════╝  ╚═╝╚═╝    ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝      ╚═════╝ 
                                                             
	]]
	if currentPlace == "Arefu" then
		local x, y, size, style, xVelocity, yVelocity, xDistance, yDistance, delay, reload, direction, arrowVelocity

		--x = 26			-- position in tiles
		--y = 29
		--size = 2		-- x tiles wide
		--style = {1, 2}	-- it's an array, every number represent a tile that will be drawn to create platform
		--xVelocity = 40
		--yVelocity = 0
		--xDistance = 5	-- how many tiles platform will move before turning back
		--yDistance = 0
		--delay = 0
--
		--local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		--table.insert( mapElements, mapElement )
--
		--x = 23			-- position in tiles
		--y = 27
		--size = 2		-- x tiles wide
		--style = {3, 1}		-- it's an array, every number represent a tile that will be drawn to create platform
		--xVelocity = 0
		--yVelocity = 10
		--xDistance = 0	-- how many tiles platform will move before turning back
		--yDistance = 3
		--delay = 0
		--
		--local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		--table.insert( mapElements, mapElement )
--
		--x = 21			-- position in tiles
		--y = 27
		--size = 1		-- x tiles wide
		--style = {2}		-- it's an array, every number represent a tile that will be drawn to create platform
		--xVelocity = 0
		--yVelocity = 10
		--xDistance = 0	-- how many tiles platform will move before turning back
		--yDistance = 3
		--delay = 1
		--
		--local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		--table.insert( mapElements, mapElement )

		--x = 16
		--y = 25
		--style = 1
		--delay = 0
		--reload = 3
		--direction = 1
		--arrowVelocity = 200
		--arrowDamage = 0
--
		--local mapElement = arrowLauncher:new(x * 16, y * 16, style, delay, reload, direction, arrowVelocity, arrowDamage)
		--table.insert( mapElements, mapElement )
	end

	--[[

	 ██████╗  ██╗       ██████╗ ██╗   ██╗███╗   ██╗ ██████╗ ███████╗ ██████╗ ███╗   ██╗ ██████╗  ██╗
	██╔═████╗███║       ██╔══██╗██║   ██║████╗  ██║██╔════╝ ██╔════╝██╔═══██╗████╗  ██║██╔═████╗███║
	██║██╔██║╚██║       ██║  ██║██║   ██║██╔██╗ ██║██║  ███╗█████╗  ██║   ██║██╔██╗ ██║██║██╔██║╚██║
	████╔╝██║ ██║       ██║  ██║██║   ██║██║╚██╗██║██║   ██║██╔══╝  ██║   ██║██║╚██╗██║████╔╝██║ ██║
	╚██████╔╝ ██║██╗    ██████╔╝╚██████╔╝██║ ╚████║╚██████╔╝███████╗╚██████╔╝██║ ╚████║╚██████╔╝ ██║
	 ╚═════╝  ╚═╝╚═╝    ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝  ╚═╝
                                                                                                                                        
	]]
	if currentPlace == "Dungeon01" then
		local x, y, size, style, xVelocity, yVelocity, xDistance, yDistance, delay, reload, direction, arrowVelocity

		x = 23
		y = 67
		style = 1
		delay = 0
		reload = 1.2
		direction = 1
		arrowVelocity = 250
		arrowDamage = 1
		
		local mapElement = arrowLauncher:new(x * 16, y * 16, style, delay, reload, direction, arrowVelocity, arrowDamage)
		table.insert( mapElements, mapElement )

		x = 17
		y = 73
		style = 1
		delay = 1
		reload = 1.2
		direction = 1
		arrowVelocity = 250
		arrowDamage = 1
		
		local mapElement = arrowLauncher:new(x * 16, y * 16, style, delay, reload, direction, arrowVelocity, arrowDamage)
		table.insert( mapElements, mapElement )

		x = 11
		y = 79
		style = 1
		delay = 2
		reload = 1.2
		direction = 1
		arrowVelocity = 250
		arrowDamage = 1
		
		local mapElement = arrowLauncher:new(x * 16, y * 16, style, delay, reload, direction, arrowVelocity, arrowDamage)
		table.insert( mapElements, mapElement )

		x = 61
		y = 14
		size = 2
		style = {1, 2}
		xVelocity = 35
		yVelocity = 0
		xDistance = 3
		yDistance = 0
		delay = 0
		
		local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		table.insert( mapElements, mapElement )

		x = 70
		y = 14
		size = 2
		style = {2, 1}
		xVelocity = 50
		yVelocity = 0
		xDistance = 6
		yDistance = 0
		delay = 0.9
		
		local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		table.insert( mapElements, mapElement )

		x = 25
		y = 98
		size = 1
		style = {2}
		xVelocity = 20
		yVelocity = 0
		xDistance = 3
		yDistance = 0
		delay = 0
		
		local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		table.insert( mapElements, mapElement )

		x = 40
		y = 98
		size = 1
		style = {2}
		xVelocity = 20
		yVelocity = 0
		xDistance = 3
		yDistance = 0
		delay = 0
		
		local mapElement = Platform:new(x * 16, y * 16, size, style, xVelocity, yVelocity, xDistance * 16, yDistance * 16, delay)
		table.insert( mapElements, mapElement )
	end
	return mapElements
end

-- Handling walls blocking way out off boss fights
function createBossWall(...)
	local args = {...}
	bossWalls = {}
	for i=1, #args / 2, 1 do
		a = 0 + (i - 1) * 2
		bossWalls[i] = {args[a + 1] - 1, args[a + 2] - 1, 0--[[ Wall height ]]}
	end
	bossWalls['ready'] = 0
	bossWalls['timer'] = 0.3
	bossWalls['state'] = "create"
		--file:write(bossWalls[1][1].." "..bossWalls[1][2])
end

function updateBossWall(dt)
	if bossWalls ~= nil then 
		if bossWalls['state'] == "create" then
			player:stop()
			player.canMove = false
			--[[
				1. wait 0.3 seconds
				2. check if there is wall underneath
					- YES - add 1 to bossWalls["ready"] - if ready == #bosswalls wszystkie ściany są na miejscu
					- NO  - put wall piece
			]]
			bossWalls['timer'] = bossWalls['timer'] - dt
			if bossWalls['timer'] <= 0 then
				bossWalls['timer'] = 0.3
				for i=1, #bossWalls, 1 do
					if isColliding(currentMap, bossWalls[i][1] * 16, (bossWalls[i][2] * 16) + 16 + (bossWalls[i][3] * 16)) then
						bossWalls['ready'] = bossWalls['ready'] + 1
						file:write(bossWalls['ready'])
					else
						bossWalls[i][3] = bossWalls[i][3] + 1
					end
				end
				if bossWalls['ready'] == #bossWalls then
					bossWalls['state'] = "closed"
					player.canMove = true
				else
					bossWalls['ready'] = 0
				end
			end
		end
		if bossWalls['state'] == "remove" then
			bossWalls['ready'] = 0
			bossWalls['timer'] = bossWalls['timer'] - dt
			if bossWalls['timer'] <= 0 then
				bossWalls['timer'] = 0.3
				for i=1, #bossWalls, 1 do
					if bossWalls[i][3] > 0 then
						bossWalls[i][3] = bossWalls[i][3] - 1
					end
					if bossWalls[i][3] <= 0 then
						bossWalls['ready'] = bossWalls['ready'] + 1
					end
				end
				if bossWalls['ready'] == #bossWalls then
					bossWalls = nil
				else
					bossWalls['ready'] = 0
				end
			end
		end
	end
end

function removeBossWall()
	bossWalls['timer'] = 0
	bossWalls['state'] = "remove"
end

function drawBossWall()
	if bossWalls ~= nil then
		for i=1, #bossWalls, 1 do
			for j=1, bossWalls[i][3], 1 do
				love.graphics.draw(bossWall, bossWalls[i][1] * 16, (bossWalls[i][2] * 16) + (j * 16))
				love.graphics.setColor(5, 5, 0, 28 * j)
				love.graphics.draw(bossWall2, bossWalls[i][1] * 16, (bossWalls[i][2] * 16) + (j * 16))
				love.graphics.setColor(255, 255, 255, 255)
			end
		end
	end
end

--function updateSpawns(dt)
--	for k,mapElement in ipairs( mapElements ) do
--		
--	end
--end