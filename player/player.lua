Player = {}

love.graphics.setDefaultFilter( 'nearest', 'nearest' )

local playerImage = love.graphics.newImage("player/sprite.png")
local playerImage2 = love.graphics.newImage("player/spriteAttack.png")

-- preparing animation
local g = anim8.newGrid(32, 64, playerImage:getWidth(), playerImage:getHeight())

--animWalkLeft	= anim8.newAnimation('loop', g('1-13,2'), 0.1, {[4]=0.12,[5]=0.14,[6]=0.14,[7]=0.14, ['10-13']=0.09}, true, nil)
--animWalkRight	= anim8.newAnimation('loop', g('1-13,2'), 0.1, {[4]=0.12,[5]=0.14,[6]=0.14,[7]=0.14, ['10-13']=0.09})
animWalkLeft	= anim8.newAnimation(g('1-8',2), 0.11):flipH()
animWalkRight	= anim8.newAnimation(g('1-8',2), 0.11)
animStandLeft	= anim8.newAnimation(g(1,1), 0.5, 'pauseAtEnd'):flipH()
animStandRight	= anim8.newAnimation(g(1,1), 0.5, 'pauseAtEnd')
animJumpUPLeft	= anim8.newAnimation(g('1-5',3), 0.06, 'pauseAtEnd'):flipH()
animJumpUPRight	= anim8.newAnimation(g('1-5',3), 0.06, 'pauseAtEnd')
animJumpDOWNLeft	= anim8.newAnimation(g('6-8',3), 0.1, 'pauseAtEnd'):flipH()
animJumpDOWNRight	= anim8.newAnimation(g('6-8',3), 0.1, 'pauseAtEnd')
animWalkUpstairsLeft	= anim8.newAnimation(g('1-10',4), 0.08):flipH()
animWalkUpstairsRight	= anim8.newAnimation(g('1-10',4), 0.08)
animWalkDownstairsLeft	= anim8.newAnimation(g('1-8',5), 0.08):flipH()
animWalkDownstairsRight	= anim8.newAnimation(g('1-8',5), 0.08)
animCrouchLeft	= anim8.newAnimation(g(1,6), 0.5, 'pauseAtEnd'):flipH()
animCrouchRight	= anim8.newAnimation(g(1,6), 0.5, 'pauseAtEnd')
animGotHit		= anim8.newAnimation(g(1,7), 0.5, 'pauseAtEnd')
animDrowning	= anim8.newAnimation(g('1-10',8), 0.05, 'pauseAtEnd')

local k = anim8.newGrid(174, 64, playerImage2:getWidth(), playerImage2:getHeight())
animAttackStandLeft		= anim8.newAnimation(k('1-3',1), 1):flipH()
animAttackStandRight	= anim8.newAnimation(k('1-3',1), 1)
animAttackJumpLeft		= anim8.newAnimation(k('1-3',2), 1):flipH()
animAttackJumpRight		= anim8.newAnimation(k('1-3',2), 1)
animAttackUpstairsLeft		= anim8.newAnimation(k('1-3',5), 1):flipH()
animAttackUpstairsRight		= anim8.newAnimation(k('1-3',5), 1)
animAttackDownstairsLeft	= anim8.newAnimation(k('1-3',4), 1):flipH()
animAttackDownstairsRight	= anim8.newAnimation(k('1-3',4), 1)
animAttackCrouchLeft		= anim8.newAnimation(k('1-3',3), 1):flipH()
animAttackCrouchRight		= anim8.newAnimation(k('1-3',3), 1)
animDyingLeft			= anim8.newAnimation(k('1-3',6), 0.3, 'pauseAtEnd'):flipH()
animDyingRight			= anim8.newAnimation(k('1-3',6), 0.3, 'pauseAtEnd')

playerAnimation = animStandRight

function Player:new()
	local object = {
	x = 0,
	y = 0,
	width = 0,
	height = 0,
	xVelocity = 0,
	yVelocity = 0,
	platformVelocity = 0, -- that's a messy way to "fix" parallax scrolling...
	xMaxVelocity = 800,
	yMaxVelocity = 750,
	walkSpeed = 0,
	breakPower = 350,
	jumpSpeed = 0,
	isJumping = false,
	isAttacking = false,
	attackTimer = 0,
	onFloor = false,
	onPlatform = false,
	onPlatform2 = false,
	hasJumped = false,
	direction = "left",
	onStairs = false,
	nearestDoor = nil,
	state = "",
	stairsPosition = 0,
	distanceFromStairs = 0,
	dialogState = nil,

	canMove = true,

	-- usunąć
	hasDagger = true,
	hasAxe = false,
	hasHoly = false,
	hasCross = false,
	subweapon = "dagger",

	soulBufferTimer = 0,
	soulBuffer = 0,
	souls = 0,
	hearts = 0,
	whipLevel = 1,
	subweaponLevel = 1,
	healthLevel = 1,

	status = "alive",
	drowning = false,
	soulTimer = 0,
	dyingTimer = 0,
	maxHealth = 4,
	health = 4,
	gotHit = false,
	invulnerableTime = 0,
	whipDamage = 1,
	whipHitbox = nil,
	hitbox = {}, -- player hitbox

	p1 = love.graphics.newParticleSystem(particleImage01, 80)
	}

	object.p1:setEmissionRate          (80)
 	object.p1:setEmitterLifetime       (1)
 	object.p1:setParticleLifetime      (1)
 	object.p1:setPosition              (0, 0)
 	object.p1:setSpeed                 (0, 1)
 	object.p1:setSizes                 (0.5, 1, 2)
 	object.p1:setSizeVariation         (0.4)
 	object.p1:setLinearAcceleration    (0, -20, 0, -50)
 	object.p1:setColors                (89, 124, 15, 240, 203, 245, 114, 240, 251, 175, 93, 200, 155, 86, 12, 50)
 	object.p1:setAreaSpread            ('normal', 6, 14)
 	object.p1:stop()

	setmetatable( object, {__index = Player})
	return object
end

-- Movement functions
function Player:moveRight()
	self.xVelocity = self.walkSpeed
	playerAnimation:resume()
end

function Player:moveLeft()
	self.xVelocity = -1 * self.walkSpeed
	playerAnimation:resume()
end

function Player:jump()
	if self.onFloor and not
			(isColliding(currentMap, self.x + 4, self.y - 33)) and not
		    (isColliding(currentMap, self.x,  self.y - 33))    and not
		    (isColliding(currentMap, self.x - 4,  self.y - 33)) then

		if love.keyboard.isDown( "a" ) then
			self.direction = "left"
			if self.xVelocity == 0 then self.xVelocity = self.walkSpeed * -1 end
		end
		if love.keyboard.isDown( "d" ) then
			self.direction = "right"
			if self.xVelocity == 0 then self.xVelocity = self.walkSpeed end
		end
		self.yVelocity = self.jumpSpeed
		self.onFloor = false
		self.onPlatform = false
		self.onPlatform2 = false
		self.isJumping = true
		self.hasJumped = true
		playerAnimation:resume()
	end
end

function Player:stop()
	self.xVelocity = 0
end

function Player:moveOnStairs(vertical, horizontal)
	self.onFloor = false
	self.onStairs = true
	if horizontal == 1 then
		self.xVelocity = self.walkSpeed
	else
		self.xVelocity = self.walkSpeed * -1
	end
	if vertical == "up" then
		self.yVelocity = self.walkSpeed * -1
	else
		self.yVelocity = self.walkSpeed
	end
end

function Player:collide(event)
	if event == "hitFloor" then
		self.yVelocity = 0
		if not self.onFloor then
			playSound("HitGround")
			if love.keyboard.isDown( "a" ) then self.xVelocity = self.walkSpeed * -1 end
			if love.keyboard.isDown( "d" ) then self.xVelocity = self.walkSpeed end
		end
		self.onFloor = true
		if self.isJumping and not 
			((love.keyboard.isDown( "a" ) or love.keyboard.isDown( "d" )) and not
			love.keyboard.isDown( "s" ) and not
			self.isAttacking) then self:stop() end
		self.isJumping = false
		self.gotHit = false

		-- death bug hax0r
		if self.status == "dead" then self:stop() end
		animJumpUPRight:gotoFrame(1)
		animJumpUPRight.status = "playing"
		animJumpUPLeft:gotoFrame(1)
		animJumpUPLeft.status = "playing"
		animJumpDOWNRight:gotoFrame(1)
		animJumpDOWNRight.status = "playing"
		animJumpDOWNLeft:gotoFrame(1)
		animJumpDOWNLeft.status = "playing"
	end
	if event == "hitCeiling" then
		self.yVelocity = 0
	end
end

-- Update player
function Player:update(dt, gravity, map)

	-- handle key presses
	if self.canMove then
		if love.keyboard.isDown( "d" )
			and self.onFloor
			and self.dialogState ~= "active"
			and self.status == "alive"
			and self.state ~= "crouchRight"
			and self.state ~= "crouchLeft"
			and not self.isAttacking
			and not self.gotHit then
			self:moveRight()
		elseif love.keyboard.isDown( "d" )
			and love.keyboard.isDown( "s" )
			and not love.keyboard.isDown( "a" )
			and self.onFloor
			and self.dialogState ~= "active"
			and self.status == "alive"
			and self.state == "crouchLeft"
			and not self.isAttacking
			and not self.gotHit then
				self.direction = "right"
		end

		if love.keyboard.isDown( "a" )
			and self.onFloor
			and self.dialogState ~= "active"
			and self.status == "alive"
			and self.state ~= "crouchRight"
			and self.state ~= "crouchLeft"
			and not self.isAttacking
			and not self.gotHit then
			self:moveLeft()
		elseif love.keyboard.isDown( "a" )
			and love.keyboard.isDown( "s" )
			and not love.keyboard.isDown( "d" )
			and self.onFloor
			and self.dialogState ~= "active"
			and self.status == "alive"
			and self.state == "crouchRight"
			and not self.isAttacking
			and not self.gotHit then
				self.direction = "left"
		end
	end

	-- moving on stairs
	if love.keyboard.isDown( "w" )
		and (self.onFloor or self.onStairs)
		and not love.keyboard.isDown( "s" )
		and not self.isAttacking
		and self.dialogState ~= "active"
		and self.status == "alive" then
		-- Find stairs!
		local order = self:findStairs(map, self.x, self.y + 31, "UP")
		self.stairsPosition = order
		if order then
			if order == 1 then
				self:moveLeft()
			end
			if order == 4 then
				self:moveRight()
			end
			if order == 3 then
				self:moveOnStairs("up", 1)
				self.x = self.x + (self.xVelocity * dt)
				self.y = self.y + (self.yVelocity * dt)
				playerAnimation:resume()
				self.direction = "right"
			end
			if order == 2 then
				self:moveOnStairs("up", 0)
				self.x = self.x + (self.xVelocity * dt)
				self.y = self.y + (self.yVelocity * dt)
				playerAnimation:resume()
				self.direction = "left"
			end
		end
		if order == 0 then
			if love.keyboard.isDown( "w" ) and not (love.keyboard.isDown( "a" ) or love.keyboard.isDown( "d" )) then self:stop() end
			self.onFloor = true
			self.onStairs = false
			self.yVelocity = 0
		end
	end
	if love.keyboard.isDown( "s" )
		and (self.onFloor or self.onStairs)
		and not love.keyboard.isDown( "w" )
		and not self.isAttacking
		and self.dialogState ~= "active"
		and self.status == "alive" then
		-- Find stairs!
		local order = self:findStairs(map, self.x, self.y + 32, "DOWN")
		self.stairsPosition = order
		if order then
			if order == 1 then
				self:moveLeft()
			end
			if order == 4 then
				self:moveRight()
			end
			if order == 5 then
				self:moveOnStairs("down", 1)
				self.x = self.x + (self.xVelocity * dt)
				self.y = self.y + (self.yVelocity * dt)
				playerAnimation:resume()
				self.direction = "right"
			end
			if order == 6 then
				self:moveOnStairs("down", 0)
				self.x = self.x + (self.xVelocity * dt)
				self.y = self.y + (self.yVelocity * dt)
				playerAnimation:resume()
				self.direction = "left"
			end
		end
		if order == 0 then
			if love.keyboard.isDown( "s" ) and not (love.keyboard.isDown( "a" ) or love.keyboard.isDown( "d" )) then self:stop() end
			self.onFloor = true
			self.onStairs = false
			self.yVelocity = 0
		end
	end

	-- update animation
	playerAnimation:update(dt)

	-- update soulTimer
	if self.soulTimer > 0 then self.soulTimer = self.soulTimer - dt end
	self.p1:update(dt)

	-- apply gravity when not on stairs
	if not(self.onStairs) then self.yVelocity = self.yVelocity + (gravity * dt) end

	-- slight air control
	if self.isJumping and love.keyboard.isDown( "a" ) and self.status == "alive" and not self.gotHit then
		self.xVelocity = self.xVelocity - (self.breakPower * dt)
	elseif self.isJumping and love.keyboard.isDown( "d" ) and self.status == "alive" and not self.gotHit then
		self.xVelocity = self.xVelocity + (self.breakPower * dt)
	end
	if self.xVelocity > 0 then
		if self.xVelocity > self.walkSpeed then self.xVelocity = self.walkSpeed end
	end
	if self.xVelocity < 0 then
		if self.xVelocity < -self.walkSpeed then self.xVelocity = -self.walkSpeed end
	end

	-- speed limit
	self.yVelocity = math.clamp(self.yVelocity, -self.yMaxVelocity, self.yMaxVelocity)

	local halfX = self.width / 2
	local halfY = self.height / 2
	-- move player
	local nextY = self.y + (self.yVelocity * dt)
	if self.drowning then nextY = self.y end
	if self.onPlatform then self.yVelocity = 0 end
	if self.yVelocity > 0 and not (self.onStairs) and not (self.onPlatform) then -- going down
		if not (isColliding(map, self.x - halfX + 8, nextY + halfY))				-- bottom left
			and not (isColliding(map, self.x, nextY + halfY))						-- bottom middle
			and not (isColliding(map, self.x + halfX - 10, nextY + halfY)) then		-- bottom right
			self.y = nextY
			-- falling cancels X movement
			if self.onFloor then
				self.xVelocity = 0 
				self.yVelocity = 100
				self.isJumping = true
			end
			self.onFloor = false
		else
			self.y = math.round(nextY - ((nextY + 16) % map.tileHeight)) -- Add the tile width NOT halfY
			self:collide("hitFloor")
		end
	elseif self.yVelocity < 0 and not (self.onStairs) then -- going up
		if not (isColliding(map, self.x - halfX + 8, nextY - halfY - 1))				-- top left
			and not (isColliding(map, self.x, nextY - halfY - 1))						-- top middle
			and not (isColliding(map, self.x + halfX - 10, nextY - halfY - 1))			-- top right
			or self.gotHit then															-- hit by enemy
			self.y = nextY
			self.onFloor = false
		else
			self.y = nextY + map.tileHeight - (nextY % map.tileHeight)
			self:collide("hitCeiling")
		end
		self.y = nextY
		self.onFloor = false
	end

	local nextX = self.x + (self.xVelocity * dt)
	if self.drowning then nextX = self.x end
	if self.xVelocity < 0 and not (self.onStairs) then
		if not (isColliding(map, nextX - halfX + 8, self.y + halfY - 1))		-- left bottom
			and not (isColliding(map, nextX - halfX + 8, self.y))				-- left middle 
			and not (isColliding(map, nextX - halfX + 8, self.y + 24))			-- left 1/4
			and not (isColliding(map, nextX - halfX + 8, self.y + 16))			-- left 2/4
			and not (isColliding(map, nextX - halfX + 8, self.y + 8 ))			-- left 3/4
			and not (isColliding(map, nextX - halfX + 8, self.y - 8 )) then		-- left 3/4
			self.x = self.x + (self.xVelocity * dt)
		else
			if self.onFloor then
				self.x = nextX + halfX - (nextX % map.tileWidth) - 8
			end
		end
	elseif self.xVelocity > 0 and not (self.onStairs) then
		if not (isColliding(map, nextX + halfX - 9, self.y + halfY - 1))		-- left bottom
			and not (isColliding(map, nextX + halfX - 9, self.y))				-- left middle 
			and not (isColliding(map, nextX + halfX - 9, self.y + 24))			-- left 1/4
			and not (isColliding(map, nextX + halfX - 9, self.y + 16))			-- left 2/4
			and not (isColliding(map, nextX + halfX - 9, self.y + 8 ))			-- left 3/4
			and not (isColliding(map, nextX + halfX - 9, self.y - 8 )) then		-- left 3/4
			self.x = self.x + (self.xVelocity * dt)
		else
			if self.onFloor then
				self.x = nextX - halfX + (map.tileWidth - (nextX % map.tileWidth)) + 9
			end
		end
	end

	-- check if you are drowning
	local tileX, tileY = math.floor(player.x / map.tileWidth), math.floor(player.y / map.tileHeight)
	local tile = map("water")(tileX, tileY)
	if not (tile == nil) and not self.drowning then
		animDrowning:gotoFrame(1)
		animDrowning.status = "playing"
		playSound("PlayerDeath")
		self.status = "dead"
		self.health = 0
		self.drowning = true
		self.dyingTimer = 1.5
		self.isJumping = false
		self.isAttacking = false
		self:stop()
	end

	-- update attack timer/animation/stuff
	if self.isAttacking then
		self.attackTimer = self.attackTimer - dt
		if self.attackTimer <= 0 then
			self.attackTimer = 0
			self.isAttacking = false
		end
	end

	-- update whipHitbox of the whip_on
	if self.isAttacking and self.attackTimer <= 0.3 then
		-- whipHitbox is array for coords | yOffset is for start point of whipHitbox | xOffset is for changing directions
		local whipHitbox, yOffset = {}, 0
		if self.state == "stand" then yOffset = 18 end
		if self.state == "jump" or self.state == "fall" then yOffset = 22 end
		if self.state == "crouchRight" or self.state == "crouchLeft" then yOffset = 4 end
		if self.state == "moveStairs" then yOffset = 20 end
		if self.direction == "right" then
			whipHitbox.x1 = player.x
			whipHitbox.x2 = player.x + 87
		else
			whipHitbox.x1 = player.x - 87
			whipHitbox.x2 = player.x
		end
		whipHitbox.y1 = player.y - yOffset
		whipHitbox.y2 = player.y - yOffset + 7
		self.whipHitbox = { whipHitbox.x1, whipHitbox.y1, whipHitbox.x2, whipHitbox.y2 }
		-- whip length is 87 pixels
	else
		self.whipHitbox = {}
	end

	-- update player hitbox and deal with taking damage
	if self.invulnerableTime > 0 then self.invulnerableTime = self.invulnerableTime - dt end
	self.hitbox[1] = self.x - 7
	self.hitbox[2] = self.y - 24
	self.hitbox[3] = self.x + 7
	self.hitbox[4] = self.y + 32
	-- change hitbox when jumping/crouching
	if self.isJumping then
		self.hitbox[4] = self.y + 16
	end
	if self.state == "crouchLeft" or self.state == "crouchRight" then
		self.hitbox[2] = self.y - 10
	end

	-- update dyingTimer
	self.dyingTimer = self.dyingTimer - dt
	if self.dyingTimer <= 0 then self.dyingTimer = 0 end

	if self.dyingTimer <= 0 and self.status == "dead" then
		gameState = "gameOverScreen"
		self.drowning = false
	end

	for k, entity in pairs( entities ) do
		-- if entity has any hitbox
		if entity.hitbox ~= nil and self.hitbox ~= nil then
			for i=1, #entity.hitbox do
				-- if hitboxes overlap
				if (self.hitbox[1] <= entity.hitbox[i][3] and self.hitbox[3] >= entity.hitbox[i][1] and
					self.hitbox[2] <= entity.hitbox[i][4] and self.hitbox[4] >= entity.hitbox[i][2]) and
					self.invulnerableTime <= 0 and
					self.status == "alive" and
					entity.status == "alive" then
					-- "Throw" the player in opposite direction
					self.gotHit = true
					local distance
					distance = self.x - entity.x
					if distance >= 0 then
						self.xVelocity = 150
					else
						self.xVelocity = -150
					end
					self.yVelocity = -180
					self.isJumping = true
					self.onFloor = false
					self.onStairs = false
					self.onPlatform = false
					self.onPlatform2 = false
					self.invulnerableTime = 1.2
					-- cancel attack
					self.isAttacking = false
					self.attackTimer = 0
					-- decrease health, play sound, die maybe
					playSound("DamageTaken")
					self.health = self.health - entity.damage
					if self.health <= 0 then
						animDyingLeft:gotoFrame(1)
						animDyingLeft.status = "playing"
						animDyingRight:gotoFrame(1)
						animDyingRight.status = "playing"
						playSound("PlayerDeath")
						self.status = "dead"
						self.dyingTimer = 2
					end
				else
					debug = 0
				end
			end
		end
	end

	-- update player state
	self.state = self:getState()
end

function Player:drawPlayer()
	if self.isAttacking or (self.status == "dead" and self.onFloor) then
		playerAnimation:draw(playerImage2, math.round(self.x) - (174 / 2), math.round(self.y) - (64 / 2))
	else
		local timeColor = (self.invulnerableTime + 0.3) * 255
		if timeColor > 255 then timeColor = 250 end
		if self.invulnerableTime > 0 then
			love.graphics.setColor(math.random(200,255),255-timeColor,255-timeColor)
		end

		playerAnimation:draw(playerImage, math.round(self.x) - (32 / 2), math.round(self.y) - (64 / 2))
		love.graphics.setColor(255, 255, 255)
	end
		love.graphics.setBlendMode("additive")
		love.graphics.draw(self.p1, player.x, player.y)
		love.graphics.setBlendMode("alpha")
	--love.graphics.rectangle("fill", self.hitbox[1], self.hitbox[2], 1, 1)
	--love.graphics.rectangle("fill", self.hitbox[3], self.hitbox[4], 1, 1)
end

function Player:spawnPlayer(x, y)
		self.x = x
		self.y = y
		self.onFloor = true
		self.onStairs = false
		camX = 1000
		camY = 100
end

function Player:findStairs(map, x, y, search)
	-- seting variables
	local playerTile, direction, offset, distance, emptyTile = math.floor(self.x / map.tileWidth), 0, 0, 0, 0
	if search == "UP" then
		-- checking first tile [x][ ][ ]
		local tileX, tileY = math.floor((x - 16) / map.tileWidth), math.floor(y / map.tileHeight)
		local tile = map("stairs")(tileX, tileY)
		if not (tile == nil) then
			if tile.properties.direction == "right" then
				if self.onStairs then	return 3 end -- moveUpRight
				offset		= 3
				direction	= 1
			else
				if self.onStairs then	return 2 end -- moveUpLeft
				offset		= -18
				direction	= 0
			end
			distance = self.x - ((playerTile - 1) * 16) + offset
			if distance < -1 then		return 4 end -- moveRight
			if distance > 1 then		return 1 end -- moveLeft
			-- KOPIA L3WL
			--if (distance <= 1) and (distance >= -1) then
			--	if direction then		return 3	 -- moveUpRight
			--	else					return 2 end -- moveUpLeft
			--end
			if (distance <= 1) and (distance >= -1) then
				if direction == 1 then	return 3	 -- moveUpRight
				else					return 2 end -- moveUpLeft
			end
		else
			emptyTile = emptyTile + 1
		end
		-- checking second tile [ ][x][ ]
		local tileX, tileY = math.floor(x / map.tileWidth), math.floor(y / map.tileHeight)
		local tile = map("stairs")(tileX, tileY)
		if not (tile == nil) then
			if tile.properties.direction == "right" then
				if self.onStairs then	return 3 end -- moveUpRight
				offset		= 3
				direction	= 1
			else
				if self.onStairs then	return 2 end -- moveUpLeft
				offset		= -18
				direction	= 0
			end
			distance = self.x - (playerTile * 16) + offset
			if distance < -1 then		return 4 end -- moveRight
			if distance > 1 then		return 1 end -- moveLeft
			if (distance <= 1) and (distance >= -1) then
				if direction == 1 then	return 3	 -- moveUpRight
				else					return 2 end -- moveUpLeft
			end
		else
			emptyTile = emptyTile + 1
		end
		-- checking third tile [ ][ ][x]
		local tileX, tileY = math.floor((x+16) / map.tileWidth), math.floor(y / map.tileHeight)
		local tile = map("stairs")(tileX, tileY)
		if not (tile == nil) then
			if tile.properties.direction == "right" then
				if self.onStairs then	return 3 end -- moveUpRight
				offset		= 3
				direction	= 1
			else
				if self.onStairs then	return 2 end -- moveUpLeft
				offset		= -18
				direction	= 0
			end
			distance = self.x - ((playerTile + 1) * 16) + offset
			if distance < -1 then		return 4 end -- moveRight
			if distance > 1 then		return 1 end -- moveLeft
			if (distance <= 1) and (distance >= -1) then
				if direction == 1 then	return 3	 -- moveUpRight
				else					return 2 end -- moveUpLeft
			end
		else
			emptyTile = emptyTile + 1
		end
		if emptyTile == 3 then return 0 end
	end

--[[	  2    3
		   \  /
		1 - 00 - 4
		   /  \
		  6    5	]]--

	if search == "DOWN" then
		-- checking first tile [x][ ][ ]
		local tileX, tileY = math.floor((x - 16) / map.tileWidth), math.floor(y / map.tileHeight)
		local tile = map("stairs")(tileX, tileY)
		if not (tile == nil) then
			if tile.properties.direction == "right" then
				if self.onStairs then	return 6 end -- moveDownLeft
				offset		= -11
				direction	= 1
			else
				if self.onStairs then	return 5 end -- moveDownRight
				offset		= -4
				direction	= 0
			end
			distance = self.x - ((playerTile - 1) * 16) + offset
			if distance < -1 then		return 4 end -- moveRight
			if distance > 1 then		return 1 end -- moveLeft
			if (distance < 1) and (distance > -1) then
				if direction then		return 6	 -- moveDownLeft
				else					return 5 end -- moveDownRight
			end
		else
			emptyTile = emptyTile + 1
		end
		-- checking second tile [ ][x][ ]
		local tileX, tileY = math.floor(x / map.tileWidth), math.floor(y / map.tileHeight)
		local tile = map("stairs")(tileX, tileY)
		if not (tile == nil) then
			if tile.properties.direction == "right" then
				if self.onStairs then	return 6 end -- moveDownLeft
				offset		= -11
				direction	= 1
			else
				if self.onStairs then	return 5 end -- moveDownRight
				offset		= -4
				direction	= 0
			end
			distance = self.x - (playerTile * 16) + offset
			if distance < -1 then		return 4 end -- moveRight
			if distance > 1 then		return 1 end -- moveLeft
			if (distance < 1) and (distance > -1) then
				if direction then		return 6	 -- moveDownLeft
				else					return 5 end -- moveDownRight
			end
		else
			emptyTile = emptyTile + 1
		end
		-- checking third tile [ ][ ][x]
		local tileX, tileY = math.floor((x+16) / map.tileWidth), math.floor(y / map.tileHeight)
		local tile = map("stairs")(tileX, tileY)
		if not (tile == nil) then
			if tile.properties.direction == "right" then
				if self.onStairs then	return 6 end -- moveDownLeft
				offset		= -11
				direction	= 1
			else
				if self.onStairs then	return 5 end -- moveDownRight
				offset		= -4
				direction	= 0
			end
			distance = self.x - ((playerTile + 1) * 16) + offset
			if distance < -1 then		return 4 end -- moveRight
			if distance > 1 then		return 1 end -- moveLeft
			if (distance < 1) and (distance > -1) then
				if direction then		return 6	 -- moveDownLeft
				else					return 5 end -- moveDownRight
			end
		else
			emptyTile = emptyTile + 1
		end
		if emptyTile == 3 then return 0 end
	end
end

function Player:getState()
	--[[ jump/fall walkRight walkLeft stand crouch ]]--
	local state = ""
	if self.onFloor then
		if self.xVelocity > 0 then
			state = "moveRight"
			self.direction = "right"
			playerAnimation = animWalkRight
		elseif self.xVelocity < 0 then
			state = "moveLeft"
			self.direction = "left"
			playerAnimation = animWalkLeft
		else
			if self.direction == "right" then
				-- if not crouching and attacking AND not pressing S
				if not (self.state == "crouchRight" and self.isAttacking) and not (love.keyboard.isDown( "s" ) and not self.isAttacking) then
					state = "stand"
					if self.isAttacking then
						playerAnimation = animAttackStandRight
						if self.attackTimer <= 0.5 then animAttackStandRight:gotoFrame(1) end
						if self.attackTimer <= 0.4 then animAttackStandRight:gotoFrame(2) end
						if self.attackTimer <= 0.3 then animAttackStandRight:gotoFrame(3) end
					else
						playerAnimation = animStandRight
					end
				end
				-- if you're pressing S but you're not attacking standing or attacking and crouching
				if (love.keyboard.isDown( "s" ) and not (self.isAttacking and self.state == "stand") and self.dialogState ~= "active") or (self.isAttacking and self.state == "crouchRight") then
					state = "crouchRight"
					if self.canMove then
						if self.isAttacking then
							playerAnimation = animAttackCrouchRight
							if self.attackTimer <= 0.4 then animAttackCrouchRight:gotoFrame(1) end
							if self.attackTimer <= 0.3 then animAttackCrouchRight:gotoFrame(2) end
							if self.attackTimer <= 0.2 then animAttackCrouchRight:gotoFrame(3) end
						else
							playerAnimation = animCrouchRight
						end
					end
				end
			else -- if direction is left
				-- if not crouching and attacking AND not pressing S
				if not (self.state == "crouchLeft" and self.isAttacking) and not (love.keyboard.isDown( "s" ) and not self.isAttacking) then
					state = "stand"
					if self.isAttacking then
						playerAnimation = animAttackStandLeft
						if self.attackTimer <= 0.4 then animAttackStandLeft:gotoFrame(1) end
						if self.attackTimer <= 0.3 then animAttackStandLeft:gotoFrame(2) end
						if self.attackTimer <= 0.2 then animAttackStandLeft:gotoFrame(3) end
					else
						playerAnimation = animStandLeft
					end
				end
				-- if you're pressing S but you're not attacking standing or attacking and crouching
				if (love.keyboard.isDown( "s" ) and not (self.isAttacking and self.state == "stand") and self.dialogState ~= "active") or (self.isAttacking and self.state == "crouchLeft") then
					state = "crouchLeft"
					if self.canMove then
						if self.isAttacking then
							playerAnimation = animAttackCrouchLeft
							if self.attackTimer <= 0.4 then animAttackCrouchLeft:gotoFrame(1) end
							if self.attackTimer <= 0.3 then animAttackCrouchLeft:gotoFrame(2) end
							if self.attackTimer <= 0.2 then animAttackCrouchLeft:gotoFrame(3) end
						else
							playerAnimation = animCrouchLeft
						end
					end
				end
			end
		end
	end
	if self.yVelocity < 0 or self.yVelocity > 0 then
		if self.yVelocity < 0 then
			if self.direction == "right" then
				if self.isAttacking then
					playerAnimation = animAttackJumpRight
					if self.attackTimer <= 0.4 then animAttackJumpRight:gotoFrame(1) end
					if self.attackTimer <= 0.3 then animAttackJumpRight:gotoFrame(2) end
					if self.attackTimer <= 0.2 then animAttackJumpRight:gotoFrame(3) end
				else
					playerAnimation = animJumpUPRight
				end
			else
				if self.isAttacking then
					playerAnimation = animAttackJumpLeft
					if self.attackTimer <= 0.4 then animAttackJumpLeft:gotoFrame(1) end
					if self.attackTimer <= 0.3 then animAttackJumpLeft:gotoFrame(2) end
					if self.attackTimer <= 0.2 then animAttackJumpLeft:gotoFrame(3) end
				else
					playerAnimation = animJumpUPLeft
				end
			end
			state = "jump"
		elseif self.yVelocity > 0 then
			if self.direction == "right" then
				if self.isAttacking then
					playerAnimation = animAttackJumpRight
					if self.attackTimer <= 0.4 then animAttackJumpRight:gotoFrame(1) end
					if self.attackTimer <= 0.3 then animAttackJumpRight:gotoFrame(2) end
					if self.attackTimer <= 0.2 then animAttackJumpRight:gotoFrame(3) end
				else
					playerAnimation = animJumpDOWNRight
				end
			else
				if self.isAttacking then
					playerAnimation = animAttackJumpLeft
					if self.attackTimer <= 0.4 then animAttackJumpLeft:gotoFrame(1) end
					if self.attackTimer <= 0.3 then animAttackJumpLeft:gotoFrame(2) end
					if self.attackTimer <= 0.2 then animAttackJumpLeft:gotoFrame(3) end
				else
					playerAnimation = animJumpDOWNLeft
				end
			end
			state = "fall"
		end
	end
	if self.onStairs then
		state = "moveStairs"
		if self.xVelocity > 0 and self.yVelocity < 0 then
			if self.isAttacking then
				playerAnimation = animAttackUpstairsRight
				if self.attackTimer <= 0.4 then animAttackUpstairsRight:gotoFrame(1) end
				if self.attackTimer <= 0.3 then animAttackUpstairsRight:gotoFrame(2) end
				if self.attackTimer <= 0.2 then animAttackUpstairsRight:gotoFrame(3) end
			else
				playerAnimation = animWalkUpstairsRight
			end
		elseif self.xVelocity < 0 and self.yVelocity < 0 then
			if self.isAttacking then
				playerAnimation = animAttackUpstairsLeft
				if self.attackTimer <= 0.4 then animAttackUpstairsLeft:gotoFrame(1) end
				if self.attackTimer <= 0.3 then animAttackUpstairsLeft:gotoFrame(2) end
				if self.attackTimer <= 0.2 then animAttackUpstairsLeft:gotoFrame(3) end
			else
				playerAnimation = animWalkUpstairsLeft
			end
		elseif self.xVelocity > 0 and self.yVelocity > 0 then
			if self.isAttacking then
				playerAnimation = animAttackDownstairsRight
				if self.attackTimer <= 0.4 then animAttackDownstairsRight:gotoFrame(1) end
				if self.attackTimer <= 0.3 then animAttackDownstairsRight:gotoFrame(2) end
				if self.attackTimer <= 0.2 then animAttackDownstairsRight:gotoFrame(3) end
			else
				playerAnimation = animWalkDownstairsRight
			end
		elseif self.xVelocity < 0 and self.yVelocity > 0 then
			if self.isAttacking then
				playerAnimation = animAttackDownstairsLeft
				if self.attackTimer <= 0.4 then animAttackDownstairsLeft:gotoFrame(1) end
				if self.attackTimer <= 0.3 then animAttackDownstairsLeft:gotoFrame(2) end
				if self.attackTimer <= 0.2 then animAttackDownstairsLeft:gotoFrame(3) end
			else
				playerAnimation = animWalkDownstairsLeft
			end
		end
	end

	if self.gotHit then
		state = "knockback"
		playerAnimation = animGotHit
	end

	if self.status == "dead" and self.onFloor then
		state = "dying"
		if self.direction == "left" then
			playerAnimation = animDyingRight
		else
			playerAnimation = animDyingLeft
		end
	end
	if self.status == "dead" and self.drowning then
		state = "dying"
		playerAnimation = animDrowning
	end

	return state
end

function Player:keypressed(key, unicode)
	if self.canMove then
		if key == "p" and self.dialogState ~= "active" and gameState == "playing" and not self.isAttacking then
			self:jump()
		end
		if key == "o"
			and not self.isAttacking
			and self.dialogState == nil
			and not self.gotHit
			and self.nearestDoor == nil
			and gameState == "playing" then
			if self.onFloor then self:stop() end
			playerAnimation:pause()
			self.isAttacking = true
			self.attackTimer = 0.4
			playSound("WhipUse")
		end
	end
end