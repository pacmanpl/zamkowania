function love.conf(t)
	t.title = "What is a man? A miserable little pile of secrets"
	t.author = "Hubert Pacan"
	t.window.width = 400
	t.window.height = 300
	t.window.fullscreen = false
	t.modules.keyboard = true
	t.modules.image = true
	t.modules.graphics = true
end